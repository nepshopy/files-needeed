-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 19, 2020 at 11:17 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepshopy_seller`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `unique_identifier` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `version` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `activated` int(1) NOT NULL DEFAULT '1',
  `image` varchar(1000) COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `set_default` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `user_id`, `address`, `country`, `city`, `postal_code`, `phone`, `set_default`, `created_at`, `updated_at`) VALUES
(1, 3, 'hlmHS734Sm', 'Afghanistan', 'BcMNW1UEPd', 'KnXwcUbSw5', '8146216865', 0, '2020-07-12 11:04:14', '2020-07-12 11:04:14'),
(2, 8, 'K4aR1zc83d', 'Nepal', 'Gr967GA03D', 'NtM5qOVSGP', '3142309476', 0, '2020-07-12 13:24:12', '2020-07-12 13:24:12'),
(3, 12, 'Imdol, Gwarko, Lalitpur.', 'Nepal', 'Lalitpur', '1231', '9851275055', 0, '2020-07-13 13:29:23', '2020-07-13 13:29:23'),
(4, 22, 'Dharam colony\r\nPalam Vihar Extension', 'Nepal', 'GURGAON', '122017', '8010198601', 0, '2020-07-16 23:35:27', '2020-07-16 23:35:27'),
(5, 26, 'qwerty', 'Nepal', 'asdf', '123123', '965465132132', 0, '2020-07-17 18:42:54', '2020-07-17 18:42:54'),
(6, 21, 'zbnm,', 'Nepal', 'xsH7MqquAE', 'iNDAB4CiJ2', 'qliBTP0zjg', 0, '2020-07-17 19:32:55', '2020-07-17 19:32:55'),
(7, 27, 'balkot', 'Nepal', 'bhaktapur', '66300', '9843083493', 1, '2020-07-17 21:15:25', '2020-07-17 21:28:18');

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `currency_format` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `name`, `logo`, `currency_id`, `currency_format`, `facebook`, `twitter`, `instagram`, `youtube`, `google_plus`, `created_at`, `updated_at`) VALUES
(1, 'Active eCommerce', 'uploads/logo/matggar.png', 1, 'symbol', 'https://facebook.com', 'https://twitter.com', 'https://instagram.com', 'https://youtube.com', 'https://google.com', '2019-08-04 16:39:15', '2019-08-04 16:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Size', '2020-02-24 05:55:07', '2020-02-24 05:55:07'),
(2, 'Fabric', '2020-02-24 05:55:13', '2020-02-24 05:55:13'),
(4, 'Memory', '2020-07-12 18:56:32', '2020-07-12 18:56:32'),
(5, 'K.G', '2020-07-12 19:25:46', '2020-07-12 19:25:46'),
(6, 'Grams', '2020-07-12 19:26:08', '2020-07-12 19:26:08'),
(7, 'Screen Size', '2020-07-12 19:26:42', '2020-07-12 19:26:42'),
(8, 'Litre', '2020-07-12 19:26:58', '2020-07-13 13:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `published` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `url`, `position`, `published`, `created_at`, `updated_at`) VALUES
(4, 'uploads/banners/W33dS1nrzSZxGIn6aqZeU9Assz9THY9qi2eBhDQh.jpeg', '#', 1, 1, '2019-03-12 05:58:23', '2020-07-13 14:07:27'),
(5, 'uploads/banners/banner.jpg', '#', 1, 0, '2019-03-12 05:58:41', '2020-07-13 14:06:52'),
(6, 'uploads/banners/vlVZqgrc96uBgYduOwMl7IOxGT2etVn31SsgY2Ex.png', '#', 2, 1, '2019-03-12 05:58:52', '2020-07-13 14:09:59'),
(7, 'uploads/banners/banner.jpg', '#', 2, 0, '2019-05-26 05:16:38', '2020-07-13 14:12:04'),
(8, 'uploads/banners/banner.jpg', '#', 2, 0, '2019-06-11 05:00:06', '2020-07-13 14:12:05'),
(9, 'uploads/banners/banner.jpg', '#', 1, 0, '2019-06-11 05:00:15', '2020-07-13 14:06:53'),
(10, 'uploads/banners/banner.jpg', '#', 1, 0, '2019-06-11 05:00:24', '2019-06-11 05:01:56'),
(11, 'uploads/banners/xodn7YlkpRVkcgXBjR93ndcMyhFMlwnLwrMCRIZt.jpeg', 'seller.nepshopy.com', 1, 1, '2020-07-13 14:08:22', '2020-07-13 14:08:51'),
(12, 'uploads/banners/oW7j0ZpCdiaO8gvRlFkYxzCrRXp9RIkIPVi0CPX5.jpeg', 'seller.nepshopy.com', 2, 1, '2020-07-13 14:12:24', '2020-07-13 14:13:45'),
(13, 'uploads/banners/UH4CEYrnQAkgjcc6H1hbLWXrzAetjQ3M0BaXKXHB.png', 'http://nepshopy.online/', 2, 1, '2020-07-13 14:14:45', '2020-07-13 14:15:24'),
(14, 'uploads/banners/N4cxCnPwWxKafPYQoNk1BeRxOrU2LQK1mmMkRTSi.jpeg', 'http://nepshopy.com/', 1, 1, '2020-07-13 14:16:11', '2020-07-13 14:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `logo`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Acer', 'uploads/brands/brand.jpg', 1, 'acer', 'acer', 'all Acer items', '2019-03-12 06:05:56', '2020-07-12 16:14:37'),
(2, 'Hp', 'uploads/brands/gkRhLj9nNAD8naSZ42KxfXRqk2qbpADYnUGEy8ja.jpeg', 1, 'hp', 'hp', 'all Hp items', '2019-03-12 06:06:13', '2020-07-12 16:14:14'),
(5, 'Dell', 'uploads/brands/Szf2fJuZ84WKoi0PAjRBQGJtWV8gY2JeWtfu0df6.jpeg', 1, 'dell', 'dell', 'all dell items', '2020-07-12 15:53:29', '2020-07-13 14:41:51'),
(6, 'Lg', 'uploads/brands/g3w02teXMGDW9Wq2CFRaN93N3QdVehjAzZnkY3J9.jpeg', 0, 'lg', 'lg', 'all Lg brand', '2020-07-12 15:53:44', '2020-07-12 16:13:11'),
(7, 'Apple', 'uploads/brands/EnnLVi7zuDK4IxO7v9gWLQXCZNXCWUKBOSn5dxst.jpeg', 1, 'apple', 'apple', 'all apple brands', '2020-07-12 15:54:03', '2020-07-12 17:01:09'),
(8, 'Samsung', 'uploads/brands/gx3X0SA35iJijE29mWpaq8pNOHQPo6rqROQnNrwS.jpeg', 1, 'samsung', 'samsung', 'all samsung brands', '2020-07-12 15:54:48', '2020-07-13 14:41:51'),
(9, 'sony', 'uploads/brands/2QkACUUp4STaL07uZXxongl3QtrqNdINGAAEHdB8.jpeg', 1, 'sony', 'sonyinc', 'sony', '2020-07-12 15:54:58', '2020-07-12 17:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `business_settings`
--

CREATE TABLE `business_settings` (
  `id` int(11) NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `business_settings`
--

INSERT INTO `business_settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'home_default_currency', '28', '2018-10-16 01:35:52', '2020-07-12 10:54:23'),
(2, 'system_default_currency', '29', '2018-10-16 01:36:58', '2020-07-17 19:43:35'),
(3, 'currency_format', '1', '2018-10-17 03:01:59', '2018-10-17 03:01:59'),
(4, 'symbol_format', '1', '2018-10-17 03:01:59', '2019-01-20 02:10:55'),
(5, 'no_of_decimals', '3', '2018-10-17 03:01:59', '2020-03-04 00:57:16'),
(6, 'product_activation', '1', '2018-10-28 01:38:37', '2019-02-04 01:11:41'),
(7, 'vendor_system_activation', '1', '2018-10-28 07:44:16', '2020-07-13 12:32:54'),
(8, 'show_vendors', '1', '2018-10-28 07:44:47', '2019-02-04 01:11:13'),
(9, 'paypal_payment', '1', '2018-10-28 07:45:16', '2020-07-12 16:53:26'),
(10, 'stripe_payment', '0', '2018-10-28 07:45:47', '2020-07-12 19:51:56'),
(11, 'cash_payment', '1', '2018-10-28 07:46:05', '2019-01-24 03:40:18'),
(12, 'payumoney_payment', '0', '2018-10-28 07:46:27', '2019-03-05 05:41:36'),
(13, 'best_selling', '1', '2018-12-24 08:13:44', '2019-02-14 05:29:13'),
(14, 'paypal_sandbox', '0', '2019-01-16 12:44:18', '2019-01-16 12:44:18'),
(15, 'sslcommerz_sandbox', '1', '2019-01-16 12:44:18', '2019-03-14 00:07:26'),
(16, 'sslcommerz_payment', '0', '2019-01-24 09:39:07', '2019-01-29 06:13:46'),
(17, 'vendor_commission', '5', '2019-01-31 06:18:04', '2020-07-12 20:06:07'),
(18, 'verification_form', '[{\"type\":\"text\",\"label\":\"Your name\"},{\"type\":\"text\",\"label\":\"Shop name\"},{\"type\":\"text\",\"label\":\"Email\"},{\"type\":\"text\",\"label\":\"PAN\\/VAT Registration Number\"},{\"type\":\"text\",\"label\":\"Full Address (Business)\"},{\"type\":\"text\",\"label\":\"Phone Number\"},{\"type\":\"file\",\"label\":\"Upload Company Registration Certificate\"},{\"type\":\"file\",\"label\":\"Upload PAN\\/VAT Certificate\"}]', '2019-02-03 11:36:58', '2020-07-14 12:33:15'),
(19, 'google_analytics', '0', '2019-02-06 12:22:35', '2019-02-06 12:22:35'),
(20, 'facebook_login', '0', '2019-02-07 12:51:59', '2020-07-13 12:00:21'),
(21, 'google_login', '0', '2019-02-07 12:52:10', '2020-07-13 12:00:22'),
(22, 'twitter_login', '0', '2019-02-07 12:52:20', '2019-02-08 02:32:56'),
(23, 'payumoney_payment', '1', '2019-03-05 11:38:17', '2019-03-05 11:38:17'),
(24, 'payumoney_sandbox', '1', '2019-03-05 11:38:17', '2019-03-05 05:39:18'),
(36, 'facebook_chat', '1', '2019-04-15 11:45:04', '2020-07-12 19:52:59'),
(37, 'email_verification', '1', '2019-04-30 07:30:07', '2020-07-17 20:55:01'),
(38, 'wallet_system', '1', '2019-05-19 08:05:44', '2020-07-12 19:51:36'),
(39, 'coupon_system', '1', '2019-06-11 09:46:18', '2020-07-12 19:51:38'),
(40, 'current_version', '3.0', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(41, 'instamojo_payment', '0', '2019-07-06 09:58:03', '2019-07-06 09:58:03'),
(42, 'instamojo_sandbox', '1', '2019-07-06 09:58:43', '2019-07-06 09:58:43'),
(43, 'razorpay', '0', '2019-07-06 09:58:43', '2019-07-06 09:58:43'),
(44, 'paystack', '0', '2019-07-21 13:00:38', '2019-07-21 13:00:38'),
(45, 'pickup_point', '0', '2019-10-17 11:50:39', '2019-10-17 11:50:39'),
(46, 'maintenance_mode', '0', '2019-10-17 11:51:04', '2020-07-12 16:52:56'),
(47, 'voguepay', '0', '2019-10-17 11:51:24', '2019-10-17 11:51:24'),
(48, 'voguepay_sandbox', '0', '2019-10-17 11:51:38', '2019-10-17 11:51:38'),
(50, 'category_wise_commission', '0', '2020-01-21 07:22:47', '2020-07-12 20:06:32'),
(51, 'conversation_system', '1', '2020-01-21 07:23:21', '2020-01-21 07:23:21'),
(52, 'guest_checkout_active', '1', '2020-01-22 07:36:38', '2020-01-22 07:36:38'),
(53, 'facebook_pixel', '1', '2020-01-22 11:43:58', '2020-07-12 19:59:54'),
(55, 'classified_product', '0', '2020-05-13 13:01:05', '2020-05-13 13:01:05'),
(56, 'pos_activation_for_seller', '1', '2020-06-11 09:45:02', '2020-06-11 09:45:02'),
(57, 'shipping_type', 'seller_wise_shipping', '2020-07-01 13:49:56', '2020-07-12 20:07:39'),
(58, 'flat_rate_shipping_cost', '50', '2020-07-01 13:49:56', '2020-07-12 20:07:19'),
(59, 'shipping_cost_admin', '50', '2020-07-01 13:49:56', '2020-07-17 19:38:49');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `variation` text COLLATE utf8_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `tax` double(8,2) DEFAULT NULL,
  `shipping_cost` double(8,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `commision_rate` double(8,2) NOT NULL DEFAULT '0.00',
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` int(1) NOT NULL DEFAULT '0',
  `top` int(1) NOT NULL DEFAULT '0',
  `digital` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `commision_rate`, `banner`, `icon`, `featured`, `top`, `digital`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Women\'s Fashion', 0.00, 'uploads/categories/banner/TZHJgFvRiYOh7sQfi2ZlD1cpYYICPbWymMqwtGKS.jpeg', 'uploads/categories/icon/8RI8T49gJyM8meXegKcEV6TGSJTJxtu0sJLHtTdF.png', 1, 1, 0, 'womensfashion', 'Women\'s Fashion', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porttitor vitae nibh ac consectetur. Nam eu accumsan sem, id fringilla enim. Nulla non orci a felis molestie posuere. Donec nisl mauris, luctus sit amet velit vel, ullamcorper vehicula risus. Duis porta ac nunc eu porta. Praesent gravida ipsum quis lorem convallis, vitae congue magna vestibulum. Pellentesque hendrerit massa magna, quis vestibulum magna dapibus eget. Aenean eget orci tincidunt, feugiat nisl nec, pharetra arcu. Etiam justo orci, molestie vel augue in, tincidunt tempor elit. Donec ut vestibulum dolor, a sagittis sem. Nam nulla felis, porta a ligula volutpat, maximus rhoncus augue. Duis sed sodales lacus. Mauris elit libero, consectetur ut purus id, laoreet pharetra nibh.', '2020-07-12 10:00:55', '2020-07-12 16:00:55'),
(2, 'Men\'s Fashion', 0.00, 'uploads/categories/banner/category-banner.jpg', 'uploads/categories/icon/h9XhWwI401u6sRoLITEk9SUMRAlWN8moGrpPfS6I.png', 1, 1, 0, 'mensfashion', 'Men\'s Fashion', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus porttitor vitae nibh ac consectetur. Nam eu accumsan sem, id fringilla enim. Nulla non orci a felis molestie posuere. Donec nisl mauris, luctus sit amet velit vel, ullamcorper vehicula risus. Duis porta ac nunc eu porta. Praesent gravida ipsum quis lorem convallis, vitae congue magna vestibulum. Pellentesque hendrerit massa magna, quis vestibulum magna dapibus eget. Aenean eget orci tincidunt, feugiat nisl nec, pharetra arcu. Etiam justo orci, molestie vel augue in, tincidunt tempor elit. Donec ut vestibulum dolor, a sagittis sem. Nam nulla felis, porta a ligula volutpat, maximus rhoncus augue. Duis sed sodales lacus. Mauris elit libero, consectetur ut purus id, laoreet pharetra nibh.', '2020-07-12 11:01:09', '2020-07-12 17:01:09'),
(3, 'Electronic Devices', 0.00, 'uploads/categories/banner/category-banner.jpg', 'uploads/categories/icon/rKAPw5rNlS84JtD9ZQqn366jwE11qyJqbzAe5yaA.png', 1, 1, 0, 'electronicdevices', 'Electronic Devices', NULL, '2020-07-13 08:08:38', '2020-07-13 14:08:38'),
(4, 'TV & Home Appliances', 0.00, 'uploads/categories/banner/1XJALCVgcBBmYux6UUcsOUMehoIhGynztU0TC3sS.jpeg', 'uploads/categories/icon/v0OOAIGcdOxb1Jpp850XSB1lvzVein8pFho7kFGV.jpeg', 0, 1, 0, 'TV--Home-Appliances-PGzMq', 'Electronic Devices', 'TV & Home Appliances', '2020-07-13 08:41:51', '2020-07-13 14:41:51'),
(5, 'Health & Beauty', 0.00, 'uploads/categories/banner/e8yOLOz7Auo4DK4ZXRsZYvAq12k4KQL1kpkWf6mj.jpeg', 'uploads/categories/icon/0MNQ6JSjNHXsqVGGU8XBRWpcKHt23QDeFKtjWKqN.jpeg', 0, 1, 0, 'Health--Beauty-oBMvU', 'Health&Beauty', 'Health & Beauty', '2020-07-13 08:41:51', '2020-07-13 14:41:51'),
(6, 'Babies & Toys', 0.00, 'uploads/categories/banner/C0w2KQ0J5X80gjbhSZufnnY8KnVRooJhn3mmLJ3y.jpeg', 'uploads/categories/icon/WzvZvTsd0gxvcGiVgDVmTwLMida5SiRLuo76tcvx.jpeg', 0, 0, 0, 'Babies--Toys-FMz8t', 'Babies&Toys', 'Babies & Toys', '2020-07-13 14:38:47', '2020-07-13 14:38:47'),
(7, 'Groceries & Pets', 0.00, 'uploads/categories/banner/lAEzs1GIAE9oyXZjCSfg1jbGeMiFftV9glwLLbq6.jpeg', 'uploads/categories/icon/Tiya1VT2rMZijruFWmA363xyN5pjiv9oyK4KOPxK.jpeg', 0, 1, 0, 'Groceries--Pets-JAvge', 'Groceries&Pets', 'Groceries & Pets', '2020-07-13 08:41:51', '2020-07-13 14:41:51'),
(8, 'Home & Lifestyle', 0.00, 'uploads/categories/banner/ZOIawsJhHLm4YJhbUab2yWV2Too0Z8UrRhmNiw9n.jpeg', 'uploads/categories/icon/PrxvKZMiKRHXwFZ7tNTDf8mTaJx09jvFiNkFrl6D.jpeg', 0, 0, 0, 'Home--Lifestyle-XT7Tc', 'Home&Lifestyle', 'Home & Lifestyle', '2020-07-13 14:39:48', '2020-07-13 14:39:48'),
(9, 'Watches & Accessories', 0.00, 'uploads/categories/banner/9aT3uYGvitsK9EDKJwZbPOeInNol0TjYmy04f8DA.jpeg', 'uploads/categories/icon/otuLEplFLuKVKVQ5xyw2TKNy3eTALHKtNwe7wWdT.jpeg', 0, 0, 0, 'Watches--Accessories-Nj1cg', 'Watches & Accessories', 'Watches & Accessories', '2020-07-13 14:40:08', '2020-07-13 14:40:08'),
(10, 'Sports & Outdoor', 0.00, 'uploads/categories/banner/mRB0mxa1U4VY5bRnYNON7TdmR2lybySOEwAEdkry.jpeg', 'uploads/categories/icon/2aFiGINm1zH5a4EEB1yHaZuWIKupNKmJ4ax8s7XH.jpeg', 0, 0, 0, 'Sports--Outdoor-Sajmt', 'Sports & Outdoor', 'Sports & Outdoor', '2020-07-13 14:40:27', '2020-07-13 14:40:27'),
(11, 'Automotive & Motorbike', 0.00, 'uploads/categories/banner/JZzCROYdkLcYyYusuHBPPkEQJHbDh2CONTo0egcC.jpeg', 'uploads/categories/icon/1M4E0tOAuoky1awBu9cjITLFxXPf4ww5HVWG5XxE.jpeg', 1, 0, 0, 'Automotive--Motorbike-dq5hj', 'Automotive & Motorbike', 'Automotive & Motorbike', '2020-07-13 08:41:11', '2020-07-13 14:41:11');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'IndianRed', '#CD5C5C', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(2, 'LightCoral', '#F08080', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(3, 'Salmon', '#FA8072', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(4, 'DarkSalmon', '#E9967A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(5, 'LightSalmon', '#FFA07A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(6, 'Crimson', '#DC143C', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(7, 'Red', '#FF0000', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(8, 'FireBrick', '#B22222', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(9, 'DarkRed', '#8B0000', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(10, 'Pink', '#FFC0CB', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(11, 'LightPink', '#FFB6C1', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(12, 'HotPink', '#FF69B4', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(13, 'DeepPink', '#FF1493', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(14, 'MediumVioletRed', '#C71585', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(15, 'PaleVioletRed', '#DB7093', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(16, 'LightSalmon', '#FFA07A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(17, 'Coral', '#FF7F50', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(18, 'Tomato', '#FF6347', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(19, 'OrangeRed', '#FF4500', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(20, 'DarkOrange', '#FF8C00', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(21, 'Orange', '#FFA500', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(22, 'Gold', '#FFD700', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(23, 'Yellow', '#FFFF00', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(24, 'LightYellow', '#FFFFE0', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(25, 'LemonChiffon', '#FFFACD', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(26, 'LightGoldenrodYellow', '#FAFAD2', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(27, 'PapayaWhip', '#FFEFD5', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(28, 'Moccasin', '#FFE4B5', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(29, 'PeachPuff', '#FFDAB9', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(30, 'PaleGoldenrod', '#EEE8AA', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(31, 'Khaki', '#F0E68C', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(32, 'DarkKhaki', '#BDB76B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(33, 'Lavender', '#E6E6FA', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(34, 'Thistle', '#D8BFD8', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(35, 'Plum', '#DDA0DD', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(36, 'Violet', '#EE82EE', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(37, 'Orchid', '#DA70D6', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(38, 'Fuchsia', '#FF00FF', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(39, 'Magenta', '#FF00FF', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(40, 'MediumOrchid', '#BA55D3', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(41, 'MediumPurple', '#9370DB', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(42, 'Amethyst', '#9966CC', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(43, 'BlueViolet', '#8A2BE2', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(44, 'DarkViolet', '#9400D3', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(45, 'DarkOrchid', '#9932CC', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(46, 'DarkMagenta', '#8B008B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(47, 'Purple', '#800080', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(48, 'Indigo', '#4B0082', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(49, 'SlateBlue', '#6A5ACD', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(50, 'DarkSlateBlue', '#483D8B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(51, 'MediumSlateBlue', '#7B68EE', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(52, 'GreenYellow', '#ADFF2F', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(53, 'Chartreuse', '#7FFF00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(54, 'LawnGreen', '#7CFC00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(55, 'Lime', '#00FF00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(56, 'LimeGreen', '#32CD32', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(57, 'PaleGreen', '#98FB98', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(58, 'LightGreen', '#90EE90', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(59, 'MediumSpringGreen', '#00FA9A', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(60, 'SpringGreen', '#00FF7F', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(61, 'MediumSeaGreen', '#3CB371', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(62, 'SeaGreen', '#2E8B57', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(63, 'ForestGreen', '#228B22', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(64, 'Green', '#008000', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(65, 'DarkGreen', '#006400', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(66, 'YellowGreen', '#9ACD32', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(67, 'OliveDrab', '#6B8E23', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(68, 'Olive', '#808000', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(69, 'DarkOliveGreen', '#556B2F', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(70, 'MediumAquamarine', '#66CDAA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(71, 'DarkSeaGreen', '#8FBC8F', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(72, 'LightSeaGreen', '#20B2AA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(73, 'DarkCyan', '#008B8B', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(74, 'Teal', '#008080', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(75, 'Aqua', '#00FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(76, 'Cyan', '#00FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(77, 'LightCyan', '#E0FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(78, 'PaleTurquoise', '#AFEEEE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(79, 'Aquamarine', '#7FFFD4', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(80, 'Turquoise', '#40E0D0', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(81, 'MediumTurquoise', '#48D1CC', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(82, 'DarkTurquoise', '#00CED1', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(83, 'CadetBlue', '#5F9EA0', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(84, 'SteelBlue', '#4682B4', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(85, 'LightSteelBlue', '#B0C4DE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(86, 'PowderBlue', '#B0E0E6', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(87, 'LightBlue', '#ADD8E6', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(88, 'SkyBlue', '#87CEEB', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(89, 'LightSkyBlue', '#87CEFA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(90, 'DeepSkyBlue', '#00BFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(91, 'DodgerBlue', '#1E90FF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(92, 'CornflowerBlue', '#6495ED', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(93, 'MediumSlateBlue', '#7B68EE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(94, 'RoyalBlue', '#4169E1', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(95, 'Blue', '#0000FF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(96, 'MediumBlue', '#0000CD', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(97, 'DarkBlue', '#00008B', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(98, 'Navy', '#000080', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(99, 'MidnightBlue', '#191970', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(100, 'Cornsilk', '#FFF8DC', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(101, 'BlanchedAlmond', '#FFEBCD', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(102, 'Bisque', '#FFE4C4', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(103, 'NavajoWhite', '#FFDEAD', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(104, 'Wheat', '#F5DEB3', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(105, 'BurlyWood', '#DEB887', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(106, 'Tan', '#D2B48C', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(107, 'RosyBrown', '#BC8F8F', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(108, 'SandyBrown', '#F4A460', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(109, 'Goldenrod', '#DAA520', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(110, 'DarkGoldenrod', '#B8860B', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(111, 'Peru', '#CD853F', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(112, 'Chocolate', '#D2691E', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(113, 'SaddleBrown', '#8B4513', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(114, 'Sienna', '#A0522D', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(115, 'Brown', '#A52A2A', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(116, 'Maroon', '#800000', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(117, 'White', '#FFFFFF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(118, 'Snow', '#FFFAFA', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(119, 'Honeydew', '#F0FFF0', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(120, 'MintCream', '#F5FFFA', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(121, 'Azure', '#F0FFFF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(122, 'AliceBlue', '#F0F8FF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(123, 'GhostWhite', '#F8F8FF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(124, 'WhiteSmoke', '#F5F5F5', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(125, 'Seashell', '#FFF5EE', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(126, 'Beige', '#F5F5DC', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(127, 'OldLace', '#FDF5E6', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(128, 'FloralWhite', '#FFFAF0', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(129, 'Ivory', '#FFFFF0', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(130, 'AntiqueWhite', '#FAEBD7', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(131, 'Linen', '#FAF0E6', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(132, 'LavenderBlush', '#FFF0F5', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(133, 'MistyRose', '#FFE4E1', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(134, 'Gainsboro', '#DCDCDC', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(135, 'LightGrey', '#D3D3D3', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(136, 'Silver', '#C0C0C0', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(137, 'DarkGray', '#A9A9A9', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(138, 'Gray', '#808080', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(139, 'DimGray', '#696969', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(140, 'LightSlateGray', '#778899', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(141, 'SlateGray', '#708090', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(142, 'DarkSlateGray', '#2F4F4F', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(143, 'Black', '#000000', '2018-11-05 02:12:30', '2018-11-05 02:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE `conversations` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `title` varchar(1000) COLLATE utf32_unicode_ci DEFAULT NULL,
  `sender_viewed` int(1) NOT NULL DEFAULT '1',
  `receiver_viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `conversations`
--

INSERT INTO `conversations` (`id`, `sender_id`, `receiver_id`, `title`, `sender_viewed`, `receiver_viewed`, `created_at`, `updated_at`) VALUES
(2, 20, 21, 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 1, 1, '2020-07-17 19:52:42', '2020-07-17 19:53:01'),
(3, 20, 21, 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 1, 1, '2020-07-17 19:52:43', '2020-07-17 19:53:31');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', 0, NULL, '2020-07-12 19:09:47'),
(2, 'AL', 'Albania', 0, NULL, '2020-07-12 19:09:47'),
(3, 'DZ', 'Algeria', 0, NULL, '2020-07-12 19:09:48'),
(4, 'DS', 'American Samoa', 0, NULL, '2020-07-12 19:09:50'),
(5, 'AD', 'Andorra', 0, NULL, '2020-07-12 19:09:51'),
(6, 'AO', 'Angola', 0, NULL, '2020-07-12 19:09:52'),
(7, 'AI', 'Anguilla', 0, NULL, '2020-07-12 19:09:53'),
(8, 'AQ', 'Antarctica', 0, NULL, '2020-07-12 19:09:54'),
(9, 'AG', 'Antigua and Barbuda', 0, NULL, '2020-07-12 19:09:55'),
(10, 'AR', 'Argentina', 0, NULL, '2020-07-12 19:09:56'),
(11, 'AM', 'Armenia', 0, NULL, '2020-07-12 19:09:58'),
(12, 'AW', 'Aruba', 0, NULL, '2020-07-12 19:09:58'),
(13, 'AU', 'Australia', 0, NULL, '2020-07-12 19:09:59'),
(14, 'AT', 'Austria', 0, NULL, '2020-07-12 19:10:00'),
(15, 'AZ', 'Azerbaijan', 0, NULL, '2020-07-12 19:10:01'),
(16, 'BS', 'Bahamas', 0, NULL, '2020-07-12 19:10:05'),
(17, 'BH', 'Bahrain', 0, NULL, '2020-07-12 19:10:06'),
(18, 'BD', 'Bangladesh', 0, NULL, '2020-07-12 19:10:08'),
(19, 'BB', 'Barbados', 0, NULL, '2020-07-12 19:10:08'),
(20, 'BY', 'Belarus', 0, NULL, '2020-07-12 19:10:09'),
(21, 'BE', 'Belgium', 0, NULL, '2020-07-12 19:10:10'),
(22, 'BZ', 'Belize', 0, NULL, '2020-07-12 19:10:10'),
(23, 'BJ', 'Benin', 0, NULL, '2020-07-12 19:10:11'),
(24, 'BM', 'Bermuda', 0, NULL, '2020-07-12 19:10:12'),
(25, 'BT', 'Bhutan', 0, NULL, '2020-07-12 19:10:13'),
(26, 'BO', 'Bolivia', 0, NULL, '2020-07-12 19:10:13'),
(27, 'BA', 'Bosnia and Herzegovina', 0, NULL, '2020-07-12 19:10:15'),
(28, 'BW', 'Botswana', 0, NULL, '2020-07-12 19:10:16'),
(29, 'BV', 'Bouvet Island', 0, NULL, '2020-07-12 19:10:17'),
(30, 'BR', 'Brazil', 0, NULL, '2020-07-12 19:10:18'),
(31, 'IO', 'British Indian Ocean Territory', 0, NULL, '2020-07-12 19:10:24'),
(32, 'BN', 'Brunei Darussalam', 0, NULL, '2020-07-12 19:10:25'),
(33, 'BG', 'Bulgaria', 0, NULL, '2020-07-12 19:10:25'),
(34, 'BF', 'Burkina Faso', 0, NULL, '2020-07-12 19:10:26'),
(35, 'BI', 'Burundi', 0, NULL, '2020-07-12 19:10:27'),
(36, 'KH', 'Cambodia', 0, NULL, '2020-07-12 19:10:27'),
(37, 'CM', 'Cameroon', 0, NULL, '2020-07-12 19:10:29'),
(38, 'CA', 'Canada', 0, NULL, '2020-07-12 19:10:29'),
(39, 'CV', 'Cape Verde', 0, NULL, '2020-07-12 19:10:30'),
(40, 'KY', 'Cayman Islands', 0, NULL, '2020-07-12 19:10:31'),
(41, 'CF', 'Central African Republic', 0, NULL, '2020-07-12 19:10:32'),
(42, 'TD', 'Chad', 0, NULL, '2020-07-12 19:10:33'),
(43, 'CL', 'Chile', 0, NULL, '2020-07-12 19:10:34'),
(44, 'CN', 'China', 0, NULL, '2020-07-12 19:10:35'),
(45, 'CX', 'Christmas Island', 0, NULL, '2020-07-12 19:10:35'),
(46, 'CC', 'Cocos (Keeling) Islands', 0, NULL, '2020-07-12 19:10:39'),
(47, 'CO', 'Colombia', 0, NULL, '2020-07-12 19:10:40'),
(48, 'KM', 'Comoros', 0, NULL, '2020-07-12 19:10:41'),
(49, 'CG', 'Congo', 0, NULL, '2020-07-12 19:10:41'),
(50, 'CK', 'Cook Islands', 0, NULL, '2020-07-12 19:10:42'),
(51, 'CR', 'Costa Rica', 0, NULL, '2020-07-12 19:10:43'),
(52, 'HR', 'Croatia (Hrvatska)', 0, NULL, '2020-07-12 19:10:44'),
(53, 'CU', 'Cuba', 0, NULL, '2020-07-12 19:10:46'),
(54, 'CY', 'Cyprus', 0, NULL, '2020-07-12 19:10:46'),
(55, 'CZ', 'Czech Republic', 0, NULL, '2020-07-12 19:10:47'),
(56, 'DK', 'Denmark', 0, NULL, '2020-07-12 19:10:48'),
(57, 'DJ', 'Djibouti', 0, NULL, '2020-07-12 19:10:49'),
(58, 'DM', 'Dominica', 0, NULL, '2020-07-12 19:10:50'),
(59, 'DO', 'Dominican Republic', 0, NULL, '2020-07-12 19:10:51'),
(60, 'TP', 'East Timor', 0, NULL, '2020-07-12 19:10:51'),
(61, 'EC', 'Ecuador', 0, NULL, '2020-07-12 19:10:55'),
(62, 'EG', 'Egypt', 0, NULL, '2020-07-12 19:10:58'),
(63, 'SV', 'El Salvador', 0, NULL, '2020-07-12 19:10:59'),
(64, 'GQ', 'Equatorial Guinea', 0, NULL, '2020-07-12 19:10:59'),
(65, 'ER', 'Eritrea', 0, NULL, '2020-07-12 19:11:00'),
(66, 'EE', 'Estonia', 0, NULL, '2020-07-12 19:11:01'),
(67, 'ET', 'Ethiopia', 0, NULL, '2020-07-12 19:11:02'),
(68, 'FK', 'Falkland Islands (Malvinas)', 0, NULL, '2020-07-12 19:11:02'),
(69, 'FO', 'Faroe Islands', 0, NULL, '2020-07-12 19:11:03'),
(70, 'FJ', 'Fiji', 0, NULL, '2020-07-12 19:11:04'),
(71, 'FI', 'Finland', 0, NULL, '2020-07-12 19:11:05'),
(72, 'FR', 'France', 0, NULL, '2020-07-12 19:11:05'),
(73, 'FX', 'France, Metropolitan', 0, NULL, '2020-07-12 19:11:06'),
(74, 'GF', 'French Guiana', 0, NULL, '2020-07-12 19:11:08'),
(75, 'PF', 'French Polynesia', 0, NULL, '2020-07-12 19:11:08'),
(76, 'TF', 'French Southern Territories', 0, NULL, '2020-07-12 19:11:12'),
(77, 'GA', 'Gabon', 0, NULL, '2020-07-12 19:11:13'),
(78, 'GM', 'Gambia', 0, NULL, '2020-07-12 19:11:13'),
(79, 'GE', 'Georgia', 0, NULL, '2020-07-12 19:11:15'),
(80, 'DE', 'Germany', 0, NULL, '2020-07-12 19:11:15'),
(81, 'GH', 'Ghana', 0, NULL, '2020-07-12 19:11:16'),
(82, 'GI', 'Gibraltar', 0, NULL, '2020-07-12 19:11:17'),
(83, 'GK', 'Guernsey', 0, NULL, '2020-07-12 19:11:18'),
(84, 'GR', 'Greece', 0, NULL, '2020-07-12 19:11:19'),
(85, 'GL', 'Greenland', 0, NULL, '2020-07-12 19:11:20'),
(86, 'GD', 'Grenada', 0, NULL, '2020-07-12 19:11:21'),
(87, 'GP', 'Guadeloupe', 0, NULL, '2020-07-12 19:11:22'),
(88, 'GU', 'Guam', 0, NULL, '2020-07-12 19:11:23'),
(89, 'GT', 'Guatemala', 0, NULL, '2020-07-12 19:11:24'),
(90, 'GN', 'Guinea', 0, NULL, '2020-07-12 19:11:25'),
(91, 'GW', 'Guinea-Bissau', 0, NULL, '2020-07-12 19:11:29'),
(92, 'GY', 'Guyana', 0, NULL, '2020-07-12 19:11:31'),
(93, 'HT', 'Haiti', 0, NULL, '2020-07-12 19:11:32'),
(94, 'HM', 'Heard and Mc Donald Islands', 0, NULL, '2020-07-12 19:11:32'),
(95, 'HN', 'Honduras', 0, NULL, '2020-07-12 19:11:33'),
(96, 'HK', 'Hong Kong', 0, NULL, '2020-07-12 19:11:34'),
(97, 'HU', 'Hungary', 0, NULL, '2020-07-12 19:11:35'),
(98, 'IS', 'Iceland', 0, NULL, '2020-07-12 19:11:36'),
(99, 'IN', 'India', 0, NULL, '2020-07-12 19:11:37'),
(100, 'IM', 'Isle of Man', 0, NULL, '2020-07-12 19:11:38'),
(101, 'ID', 'Indonesia', 0, NULL, '2020-07-12 19:11:39'),
(102, 'IR', 'Iran (Islamic Republic of)', 0, NULL, '2020-07-12 19:11:40'),
(103, 'IQ', 'Iraq', 0, NULL, '2020-07-12 19:11:40'),
(104, 'IE', 'Ireland', 0, NULL, '2020-07-12 19:11:42'),
(105, 'IL', 'Israel', 0, NULL, '2020-07-12 19:11:42'),
(106, 'IT', 'Italy', 0, NULL, '2020-07-12 19:11:46'),
(107, 'CI', 'Ivory Coast', 0, NULL, '2020-07-12 19:11:48'),
(108, 'JE', 'Jersey', 0, NULL, '2020-07-12 19:11:48'),
(109, 'JM', 'Jamaica', 0, NULL, '2020-07-12 19:11:50'),
(110, 'JP', 'Japan', 0, NULL, '2020-07-12 19:11:51'),
(111, 'JO', 'Jordan', 0, NULL, '2020-07-12 19:11:51'),
(112, 'KZ', 'Kazakhstan', 0, NULL, '2020-07-12 19:11:52'),
(113, 'KE', 'Kenya', 0, NULL, '2020-07-12 19:11:53'),
(114, 'KI', 'Kiribati', 0, NULL, '2020-07-12 19:11:53'),
(115, 'KP', 'Korea, Democratic People\'s Republic of', 0, NULL, '2020-07-12 19:11:54'),
(116, 'KR', 'Korea, Republic of', 0, NULL, '2020-07-12 19:11:55'),
(117, 'XK', 'Kosovo', 0, NULL, '2020-07-12 19:11:56'),
(118, 'KW', 'Kuwait', 0, NULL, '2020-07-12 19:11:57'),
(119, 'KG', 'Kyrgyzstan', 0, NULL, '2020-07-12 19:11:59'),
(120, 'LA', 'Lao People\'s Democratic Republic', 0, NULL, '2020-07-12 19:11:59'),
(121, 'LV', 'Latvia', 0, NULL, '2020-07-12 19:12:05'),
(122, 'LB', 'Lebanon', 0, NULL, '2020-07-12 19:12:06'),
(123, 'LS', 'Lesotho', 0, NULL, '2020-07-12 19:12:07'),
(124, 'LR', 'Liberia', 0, NULL, '2020-07-12 19:12:07'),
(125, 'LY', 'Libyan Arab Jamahiriya', 0, NULL, '2020-07-12 19:12:08'),
(126, 'LI', 'Liechtenstein', 0, NULL, '2020-07-12 19:12:10'),
(127, 'LT', 'Lithuania', 0, NULL, '2020-07-12 19:12:11'),
(128, 'LU', 'Luxembourg', 0, NULL, '2020-07-12 19:12:12'),
(129, 'MO', 'Macau', 0, NULL, '2020-07-12 19:12:13'),
(130, 'MK', 'Macedonia', 0, NULL, '2020-07-12 19:12:14'),
(131, 'MG', 'Madagascar', 0, NULL, '2020-07-12 19:12:14'),
(132, 'MW', 'Malawi', 0, NULL, '2020-07-12 19:12:15'),
(133, 'MY', 'Malaysia', 0, NULL, '2020-07-12 19:12:17'),
(134, 'MV', 'Maldives', 0, NULL, '2020-07-12 19:12:18'),
(135, 'ML', 'Mali', 0, NULL, '2020-07-12 19:12:19'),
(136, 'MT', 'Malta', 0, NULL, '2020-07-12 19:12:25'),
(137, 'MH', 'Marshall Islands', 0, NULL, '2020-07-12 19:12:26'),
(138, 'MQ', 'Martinique', 0, NULL, '2020-07-12 19:12:27'),
(139, 'MR', 'Mauritania', 0, NULL, '2020-07-12 19:12:27'),
(140, 'MU', 'Mauritius', 0, NULL, '2020-07-12 19:12:28'),
(141, 'TY', 'Mayotte', 0, NULL, '2020-07-12 19:12:29'),
(142, 'MX', 'Mexico', 0, NULL, '2020-07-12 19:12:29'),
(143, 'FM', 'Micronesia, Federated States of', 0, NULL, '2020-07-12 19:12:30'),
(144, 'MD', 'Moldova, Republic of', 0, NULL, '2020-07-12 19:12:31'),
(145, 'MC', 'Monaco', 0, NULL, '2020-07-12 19:12:31'),
(146, 'MN', 'Mongolia', 0, NULL, '2020-07-12 19:12:33'),
(147, 'ME', 'Montenegro', 0, NULL, '2020-07-12 19:12:33'),
(148, 'MS', 'Montserrat', 0, NULL, '2020-07-12 19:12:34'),
(149, 'MA', 'Morocco', 0, NULL, '2020-07-12 19:12:37'),
(150, 'MZ', 'Mozambique', 0, NULL, '2020-07-12 19:12:38'),
(151, 'MM', 'Myanmar', 0, NULL, '2020-07-12 19:12:42'),
(152, 'NA', 'Namibia', 0, NULL, '2020-07-12 19:12:43'),
(153, 'NR', 'Nauru', 0, NULL, '2020-07-12 19:12:43'),
(154, 'NP', 'Nepal', 1, NULL, '2020-07-12 19:13:00'),
(155, 'NL', 'Netherlands', 0, NULL, '2020-07-12 19:12:45'),
(156, 'AN', 'Netherlands Antilles', 0, NULL, '2020-07-12 19:12:46'),
(157, 'NC', 'New Caledonia', 0, NULL, '2020-07-12 19:12:46'),
(158, 'NZ', 'New Zealand', 0, NULL, '2020-07-12 19:12:47'),
(159, 'NI', 'Nicaragua', 0, NULL, '2020-07-12 19:12:48'),
(160, 'NE', 'Niger', 0, NULL, '2020-07-12 19:12:49'),
(161, 'NG', 'Nigeria', 0, NULL, '2020-07-12 19:12:52'),
(162, 'NU', 'Niue', 0, NULL, '2020-07-12 19:12:53'),
(163, 'NF', 'Norfolk Island', 0, NULL, '2020-07-12 19:12:54'),
(164, 'MP', 'Northern Mariana Islands', 0, NULL, '2020-07-12 19:12:55'),
(165, 'NO', 'Norway', 0, NULL, '2020-07-12 19:12:55'),
(166, 'OM', 'Oman', 0, NULL, '2020-07-12 19:13:09'),
(167, 'PK', 'Pakistan', 0, NULL, '2020-07-12 19:13:12'),
(168, 'PW', 'Palau', 0, NULL, '2020-07-12 19:13:12'),
(169, 'PS', 'Palestine', 0, NULL, '2020-07-12 19:13:13'),
(170, 'PA', 'Panama', 0, NULL, '2020-07-12 19:13:14'),
(171, 'PG', 'Papua New Guinea', 0, NULL, '2020-07-12 19:13:15'),
(172, 'PY', 'Paraguay', 0, NULL, '2020-07-12 19:13:16'),
(173, 'PE', 'Peru', 0, NULL, '2020-07-12 19:13:17'),
(174, 'PH', 'Philippines', 0, NULL, '2020-07-12 19:13:17'),
(175, 'PN', 'Pitcairn', 0, NULL, '2020-07-12 19:13:18'),
(176, 'PL', 'Poland', 0, NULL, '2020-07-12 19:13:18'),
(177, 'PT', 'Portugal', 0, NULL, '2020-07-12 19:13:19'),
(178, 'PR', 'Puerto Rico', 0, NULL, '2020-07-12 19:13:20'),
(179, 'QA', 'Qatar', 0, NULL, '2020-07-12 19:13:22'),
(180, 'RE', 'Reunion', 0, NULL, '2020-07-12 19:13:23'),
(181, 'RO', 'Romania', 0, NULL, '2020-07-12 19:13:27'),
(182, 'RU', 'Russian Federation', 0, NULL, '2020-07-12 19:13:29'),
(183, 'RW', 'Rwanda', 0, NULL, '2020-07-12 19:13:29'),
(184, 'KN', 'Saint Kitts and Nevis', 0, NULL, '2020-07-12 19:13:29'),
(185, 'LC', 'Saint Lucia', 0, NULL, '2020-07-12 19:13:30'),
(186, 'VC', 'Saint Vincent and the Grenadines', 0, NULL, '2020-07-12 19:13:31'),
(187, 'WS', 'Samoa', 0, NULL, '2020-07-12 19:13:32'),
(188, 'SM', 'San Marino', 0, NULL, '2020-07-12 19:13:32'),
(189, 'ST', 'Sao Tome and Principe', 0, NULL, '2020-07-12 19:13:33'),
(190, 'SA', 'Saudi Arabia', 0, NULL, '2020-07-12 19:13:34'),
(191, 'SN', 'Senegal', 0, NULL, '2020-07-12 19:13:35'),
(192, 'RS', 'Serbia', 0, NULL, '2020-07-12 19:13:36'),
(193, 'SC', 'Seychelles', 0, NULL, '2020-07-12 19:13:37'),
(194, 'SL', 'Sierra Leone', 0, NULL, '2020-07-12 19:13:38'),
(195, 'SG', 'Singapore', 0, NULL, '2020-07-12 19:13:39'),
(196, 'SK', 'Slovakia', 0, NULL, '2020-07-12 19:13:44'),
(197, 'SI', 'Slovenia', 0, NULL, '2020-07-12 19:13:45'),
(198, 'SB', 'Solomon Islands', 0, NULL, '2020-07-12 19:13:45'),
(199, 'SO', 'Somalia', 0, NULL, '2020-07-12 19:13:46'),
(200, 'ZA', 'South Africa', 0, NULL, '2020-07-12 19:13:46'),
(201, 'GS', 'South Georgia South Sandwich Islands', 0, NULL, '2020-07-12 19:13:47'),
(202, 'SS', 'South Sudan', 0, NULL, '2020-07-12 19:13:48'),
(203, 'ES', 'Spain', 0, NULL, '2020-07-12 19:13:49'),
(204, 'LK', 'Sri Lanka', 0, NULL, '2020-07-12 19:13:49'),
(205, 'SH', 'St. Helena', 0, NULL, '2020-07-12 19:13:50'),
(206, 'PM', 'St. Pierre and Miquelon', 0, NULL, '2020-07-12 19:13:51'),
(207, 'SD', 'Sudan', 0, NULL, '2020-07-12 19:13:52'),
(208, 'SR', 'Suriname', 0, NULL, '2020-07-12 19:13:54'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands', 0, NULL, '2020-07-12 19:13:54'),
(210, 'SZ', 'Swaziland', 0, NULL, '2020-07-12 19:13:55'),
(211, 'SE', 'Sweden', 0, NULL, '2020-07-12 19:13:59'),
(212, 'CH', 'Switzerland', 0, NULL, '2020-07-12 19:14:00'),
(213, 'SY', 'Syrian Arab Republic', 0, NULL, '2020-07-12 19:14:00'),
(214, 'TW', 'Taiwan', 0, NULL, '2020-07-12 19:14:01'),
(215, 'TJ', 'Tajikistan', 0, NULL, '2020-07-12 19:14:02'),
(216, 'TZ', 'Tanzania, United Republic of', 0, NULL, '2020-07-12 19:14:02'),
(217, 'TH', 'Thailand', 0, NULL, '2020-07-12 19:14:03'),
(218, 'TG', 'Togo', 0, NULL, '2020-07-12 19:14:03'),
(219, 'TK', 'Tokelau', 0, NULL, '2020-07-12 19:14:04'),
(220, 'TO', 'Tonga', 0, NULL, '2020-07-12 19:14:05'),
(221, 'TT', 'Trinidad and Tobago', 0, NULL, '2020-07-12 19:14:05'),
(222, 'TN', 'Tunisia', 0, NULL, '2020-07-12 19:14:06'),
(223, 'TR', 'Turkey', 0, NULL, '2020-07-12 19:14:08'),
(224, 'TM', 'Turkmenistan', 0, NULL, '2020-07-12 19:14:09'),
(225, 'TC', 'Turks and Caicos Islands', 0, NULL, '2020-07-12 19:14:09'),
(226, 'TV', 'Tuvalu', 0, NULL, '2020-07-12 19:14:12'),
(227, 'UG', 'Uganda', 0, NULL, '2020-07-12 19:14:13'),
(228, 'UA', 'Ukraine', 0, NULL, '2020-07-12 19:14:14'),
(229, 'AE', 'United Arab Emirates', 0, NULL, '2020-07-12 19:14:14'),
(230, 'GB', 'United Kingdom', 0, NULL, '2020-07-12 19:14:15'),
(231, 'US', 'United States', 0, NULL, '2020-07-12 19:14:16'),
(232, 'UM', 'United States minor outlying islands', 0, NULL, '2020-07-12 19:14:16'),
(233, 'UY', 'Uruguay', 0, NULL, '2020-07-12 19:14:18'),
(234, 'UZ', 'Uzbekistan', 0, NULL, '2020-07-12 19:14:19'),
(235, 'VU', 'Vanuatu', 0, NULL, '2020-07-12 19:14:20'),
(236, 'VA', 'Vatican City State', 0, NULL, '2020-07-12 19:14:22'),
(237, 'VE', 'Venezuela', 0, NULL, '2020-07-12 19:14:23'),
(238, 'VN', 'Vietnam', 0, NULL, '2020-07-12 19:14:23'),
(239, 'VG', 'Virgin Islands (British)', 0, NULL, '2020-07-12 19:14:24'),
(240, 'VI', 'Virgin Islands (U.S.)', 0, NULL, '2020-07-12 19:14:24'),
(241, 'WF', 'Wallis and Futuna Islands', 0, NULL, '2020-07-12 19:14:29'),
(242, 'EH', 'Western Sahara', 0, NULL, '2020-07-12 19:14:29'),
(243, 'YE', 'Yemen', 0, NULL, '2020-07-12 19:14:30'),
(244, 'ZR', 'Zaire', 0, NULL, '2020-07-12 19:14:31'),
(245, 'ZM', 'Zambia', 0, NULL, '2020-07-12 19:14:32'),
(246, 'ZW', 'Zimbabwe', 0, NULL, '2020-07-12 19:14:32'),
(247, 'AF', 'Afghanistan', 0, NULL, '2020-07-12 19:14:33'),
(248, 'AL', 'Albania', 0, NULL, '2020-07-12 19:14:33'),
(249, 'DZ', 'Algeria', 0, NULL, '2020-07-12 19:14:34'),
(250, 'DS', 'American Samoa', 0, NULL, '2020-07-12 19:14:36'),
(251, 'AD', 'Andorra', 0, NULL, '2020-07-12 19:14:37'),
(252, 'AO', 'Angola', 0, NULL, '2020-07-12 19:14:37'),
(253, 'AI', 'Anguilla', 0, NULL, '2020-07-12 19:14:39'),
(254, 'AQ', 'Antarctica', 0, NULL, '2020-07-12 19:14:39'),
(255, 'AG', 'Antigua and Barbuda', 0, NULL, '2020-07-12 19:14:40'),
(256, 'AR', 'Argentina', 0, NULL, '2020-07-12 19:14:43'),
(257, 'AM', 'Armenia', 0, NULL, '2020-07-12 19:14:44'),
(258, 'AW', 'Aruba', 0, NULL, '2020-07-12 19:14:44'),
(259, 'AU', 'Australia', 0, NULL, '2020-07-12 19:14:45'),
(260, 'AT', 'Austria', 0, NULL, '2020-07-12 19:14:46'),
(261, 'AZ', 'Azerbaijan', 0, NULL, '2020-07-12 19:14:46'),
(262, 'BS', 'Bahamas', 0, NULL, '2020-07-12 19:14:47'),
(263, 'BH', 'Bahrain', 0, NULL, '2020-07-12 19:14:48'),
(264, 'BD', 'Bangladesh', 0, NULL, '2020-07-12 19:14:49'),
(265, 'BB', 'Barbados', 0, NULL, '2020-07-12 19:14:50'),
(266, 'BY', 'Belarus', 0, NULL, '2020-07-12 19:14:52'),
(267, 'BE', 'Belgium', 0, NULL, '2020-07-12 19:14:52'),
(268, 'BZ', 'Belize', 0, NULL, '2020-07-12 19:14:53'),
(269, 'BJ', 'Benin', 0, NULL, '2020-07-12 19:14:55'),
(270, 'BM', 'Bermuda', 0, NULL, '2020-07-12 19:14:55'),
(271, 'BT', 'Bhutan', 0, NULL, '2020-07-12 19:14:59'),
(272, 'BO', 'Bolivia', 0, NULL, '2020-07-12 19:15:00'),
(273, 'BA', 'Bosnia and Herzegovina', 0, NULL, '2020-07-12 19:15:01'),
(274, 'BW', 'Botswana', 0, NULL, '2020-07-12 19:15:02'),
(275, 'BV', 'Bouvet Island', 0, NULL, '2020-07-12 19:15:02'),
(276, 'BR', 'Brazil', 0, NULL, '2020-07-12 19:15:03'),
(277, 'IO', 'British Indian Ocean Territory', 0, NULL, '2020-07-12 19:15:04'),
(278, 'BN', 'Brunei Darussalam', 0, NULL, '2020-07-12 19:15:04'),
(279, 'BG', 'Bulgaria', 0, NULL, '2020-07-12 19:15:05'),
(280, 'BF', 'Burkina Faso', 0, NULL, '2020-07-12 19:15:06'),
(281, 'BI', 'Burundi', 0, NULL, '2020-07-12 19:15:07'),
(282, 'KH', 'Cambodia', 0, NULL, '2020-07-12 19:15:08'),
(283, 'CM', 'Cameroon', 0, NULL, '2020-07-12 19:15:09'),
(284, 'CA', 'Canada', 0, NULL, '2020-07-12 19:15:11'),
(285, 'CV', 'Cape Verde', 0, NULL, '2020-07-12 19:15:11'),
(286, 'KY', 'Cayman Islands', 0, NULL, '2020-07-12 19:15:16'),
(287, 'CF', 'Central African Republic', 0, NULL, '2020-07-12 19:15:17'),
(288, 'TD', 'Chad', 0, NULL, '2020-07-12 19:15:18'),
(289, 'CL', 'Chile', 0, NULL, '2020-07-12 19:15:18'),
(290, 'CN', 'China', 0, NULL, '2020-07-12 19:15:19'),
(291, 'CX', 'Christmas Island', 0, NULL, '2020-07-12 19:15:20'),
(292, 'CC', 'Cocos (Keeling) Islands', 0, NULL, '2020-07-12 19:15:21'),
(293, 'CO', 'Colombia', 0, NULL, '2020-07-12 19:15:22'),
(294, 'KM', 'Comoros', 0, NULL, '2020-07-12 19:15:23'),
(295, 'CG', 'Congo', 0, NULL, '2020-07-12 19:15:23'),
(296, 'CK', 'Cook Islands', 0, NULL, '2020-07-12 19:15:24');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL,
  `discount_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(15) NOT NULL,
  `end_date` int(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `type`, `code`, `details`, `discount`, `discount_type`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'cart_base', 'HAPPY', '{\"min_buy\":\"1500\",\"max_discount\":\"250\"}', 10.00, 'percent', 1594512000, 1622419200, '2020-07-12 19:17:45', '2020-07-12 19:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_usages`
--

CREATE TABLE `coupon_usages` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` double(10,5) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `exchange_rate`, `status`, `code`, `created_at`, `updated_at`) VALUES
(1, 'U.S. Dollar', '$', 1.00000, 1, 'USD', '2018-10-09 11:35:08', '2018-10-17 05:50:52'),
(2, 'Australian Dollar', '$', 1.28000, 1, 'AUD', '2018-10-09 11:35:08', '2019-02-04 05:51:55'),
(5, 'Brazilian Real', 'R$', 3.25000, 1, 'BRL', '2018-10-09 11:35:08', '2018-10-17 05:51:00'),
(6, 'Canadian Dollar', '$', 1.27000, 1, 'CAD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(7, 'Czech Koruna', 'Kč', 20.65000, 0, 'CZK', '2018-10-09 11:35:08', '2020-07-17 19:44:49'),
(8, 'Danish Krone', 'kr', 6.05000, 0, 'DKK', '2018-10-09 11:35:08', '2020-07-17 19:44:48'),
(9, 'Euro', '€', 0.85000, 1, 'EUR', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(10, 'Hong Kong Dollar', '$', 7.83000, 1, 'HKD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(11, 'Hungarian Forint', 'Ft', 255.24000, 0, 'HUF', '2018-10-09 11:35:08', '2020-07-17 19:44:45'),
(12, 'Israeli New Sheqel', '₪', 3.48000, 0, 'ILS', '2018-10-09 11:35:08', '2020-07-17 19:44:44'),
(13, 'Japanese Yen', '¥', 107.12000, 1, 'JPY', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(14, 'Malaysian Ringgit', 'RM', 3.91000, 0, 'MYR', '2018-10-09 11:35:08', '2020-07-17 19:44:39'),
(15, 'Mexican Peso', '$', 18.72000, 0, 'MXN', '2018-10-09 11:35:08', '2020-07-17 19:44:39'),
(16, 'Norwegian Krone', 'kr', 7.83000, 0, 'NOK', '2018-10-09 11:35:08', '2020-07-17 19:44:38'),
(17, 'New Zealand Dollar', '$', 1.38000, 1, 'NZD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(18, 'Philippine Peso', '₱', 52.26000, 0, 'PHP', '2018-10-09 11:35:08', '2020-07-17 19:44:36'),
(19, 'Polish Zloty', 'zł', 3.39000, 0, 'PLN', '2018-10-09 11:35:08', '2020-07-17 19:44:34'),
(20, 'Pound Sterling', '£', 0.72000, 1, 'GBP', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(21, 'Russian Ruble', 'руб', 55.93000, 0, 'RUB', '2018-10-09 11:35:08', '2020-07-17 19:44:30'),
(22, 'Singapore Dollar', '$', 1.32000, 1, 'SGD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(23, 'Swedish Krona', 'kr', 8.19000, 0, 'SEK', '2018-10-09 11:35:08', '2020-07-17 19:44:26'),
(24, 'Swiss Franc', 'CHF', 0.94000, 0, 'CHF', '2018-10-09 11:35:08', '2020-07-17 19:44:24'),
(26, 'Thai Baht', '฿', 31.39000, 0, 'THB', '2018-10-09 11:35:08', '2020-07-17 19:44:23'),
(27, 'Taka', '৳', 84.00000, 0, 'BDT', '2018-10-09 11:35:08', '2020-07-17 19:44:22'),
(28, 'Indian Rupee', 'Rs', 68.45000, 1, 'Rupee', '2019-07-07 10:33:46', '2019-07-07 10:33:46'),
(29, 'Nepali', 'Rs.', 120.46000, 1, 'NPR', '2020-07-17 19:22:55', '2020-07-17 19:55:21');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(4, 8, '2019-08-01 10:35:09', '2019-08-01 10:35:09'),
(6, 19, '2020-07-13 11:12:32', '2020-07-13 11:12:32'),
(7, 20, '2020-07-13 12:00:47', '2020-07-13 12:00:47'),
(8, 23, '2020-07-16 23:50:39', '2020-07-16 23:50:39'),
(9, 24, '2020-07-16 23:54:57', '2020-07-16 23:54:57'),
(10, 25, '2020-07-17 11:04:37', '2020-07-17 11:04:37');

-- --------------------------------------------------------

--
-- Table structure for table `customer_packages`
--

CREATE TABLE `customer_packages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` double(28,2) DEFAULT NULL,
  `product_upload` int(11) DEFAULT NULL,
  `logo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer_packages`
--

INSERT INTO `customer_packages` (`id`, `name`, `amount`, `product_upload`, `logo`, `created_at`, `updated_at`) VALUES
(3, 'yvmpo1whHY', 1000.00, 919731, 'uploads/customer_package/LtdwZjBkmcH60NjlYaH9PfL7KuKwrbZkW5VkTuPH.jpeg', '2020-07-13 11:07:30', '2020-07-13 11:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `customer_products`
--

CREATE TABLE `customer_products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `added_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `subsubcategory_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `photos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_img` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conditon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci,
  `video_provider` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `unit_price` double(28,2) DEFAULT '0.00',
  `meta_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_img` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flash_deals`
--

CREATE TABLE `flash_deals` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` int(20) DEFAULT NULL,
  `end_date` int(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `featured` int(1) NOT NULL DEFAULT '0',
  `background_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `flash_deals`
--

INSERT INTO `flash_deals` (`id`, `title`, `start_date`, `end_date`, `status`, `featured`, `background_color`, `text_color`, `banner`, `slug`, `created_at`, `updated_at`) VALUES
(7, 'HykNHmUZO1', 1595182500, 1596478500, 1, 1, '#ffff', 'white', 'uploads/offers/banner/G8np30UKyTq3s585Eefy9Rh6Pf6jEzTTFWiHg0qi.png', 'hyknhmuzo1-s6327', '2020-07-17 19:16:22', '2020-07-17 19:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `flash_deal_products`
--

CREATE TABLE `flash_deal_products` (
  `id` int(11) NOT NULL,
  `flash_deal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount` double(8,2) DEFAULT '0.00',
  `discount_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `flash_deal_products`
--

INSERT INTO `flash_deal_products` (`id`, `flash_deal_id`, `product_id`, `discount`, `discount_type`, `created_at`, `updated_at`) VALUES
(9, 7, 16, 7.00, 'percent', '2020-07-17 19:16:22', '2020-07-17 19:16:22'),
(10, 7, 18, 0.00, 'amount', '2020-07-17 19:16:22', '2020-07-17 19:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `frontend_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_sidebar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `frontend_color`, `logo`, `admin_logo`, `admin_login_background`, `admin_login_sidebar`, `favicon`, `site_name`, `address`, `description`, `phone`, `email`, `facebook`, `instagram`, `twitter`, `youtube`, `google_plus`, `created_at`, `updated_at`) VALUES
(1, '6', 'uploads/logo/Uqp2VYFEp37S2Jgj7XzVGjBfJXwaFIxnBESOOMTO.png', 'uploads/admin_logo/cJ6a62naEOEgNg8oZWqMl5LtXUa8zkyZjdlJ7QCo.png', 'uploads/admin_login_background/3s6cLSitqjsizabz9Ts1YUL55ywpR9POcnaDlTdX.jpeg', 'uploads/admin_login_sidebar/m7qD6ebYnGFv4wHkeZj1QHf6SzHIDk3cx8YV1HGL.jpeg', 'uploads/favicon/QdGnGVXVrrd4lAJUWRKP86khV5PYF6aOGdWrTAz1.png', 'Nep Shopy', 'Gwarkho, lalitpur', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', '9851275055', 'nepshopy@gmail.com', 'https://www.facebook.com/nepshopynp', 'https://www.instagram.com/nepshopynp', 'https://www.twitter.com/nepshopy1', 'https://www.youtube.com/nepshopynp', 'https://www.googleplus.com/nepshopynp', '2020-07-12 13:51:15', '2020-07-12 19:51:15');

-- --------------------------------------------------------

--
-- Table structure for table `home_categories`
--

CREATE TABLE `home_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subsubcategories` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_categories`
--

INSERT INTO `home_categories` (`id`, `category_id`, `subsubcategories`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '[\"1\"]', 1, '2019-03-12 06:38:23', '2019-03-12 06:38:23'),
(2, 2, '[\"10\"]', 1, '2019-03-12 06:44:54', '2019-03-12 06:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rtl` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `rtl`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 0, '2019-01-20 12:13:20', '2019-01-20 12:13:20'),
(3, 'Bangla', 'bd', 0, '2019-02-17 06:35:37', '2019-02-18 06:49:51'),
(4, 'Arabic', 'sa', 1, '2019-04-28 18:34:12', '2019-04-28 18:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `conversation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf32_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `conversation_id`, `user_id`, `message`, `created_at`, `updated_at`) VALUES
(8, 2, 20, 'http://seller.nepshopy.com/product/Brown-Lace-Up-Glossy-Formal-Shoes-For-Men-G-917-Kd9j0\r\nHello test', '2020-07-17 19:52:42', '2020-07-17 19:52:42'),
(9, 3, 20, 'http://seller.nepshopy.com/product/Brown-Lace-Up-Glossy-Formal-Shoes-For-Men-G-917-Kd9j0\r\nHello test', '2020-07-17 19:52:43', '2020-07-17 19:52:43'),
(10, 2, 21, 'hii', '2020-07-17 19:53:19', '2020-07-17 19:53:19'),
(11, 2, 20, 'Is my product ready', '2020-07-17 19:53:58', '2020-07-17 19:53:58'),
(12, 2, 21, 'it is indeed ready.', '2020-07-17 19:54:38', '2020-07-17 19:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('125ce8289850f80d9fea100325bf892fbd0deba1f87dbfc2ab81fb43d57377ec24ed65f7dc560e46', 1, 1, 'Personal Access Token', '[]', 0, '2019-07-30 04:51:13', '2019-07-30 04:51:13', '2020-07-30 10:51:13'),
('293d2bb534220c070c4e90d25b5509965d23d3ddbc05b1e29fb4899ae09420ff112dbccab1c6f504', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 06:00:04', '2019-08-04 06:00:04', '2020-08-04 12:00:04'),
('5363e91c7892acdd6417aa9c7d4987d83568e229befbd75be64282dbe8a88147c6c705e06c1fb2bf', 1, 1, 'Personal Access Token', '[]', 0, '2019-07-13 06:44:28', '2019-07-13 06:44:28', '2020-07-13 12:44:28'),
('681b4a4099fac5e12517307b4027b54df94cbaf0cbf6b4bf496534c94f0ccd8a79dd6af9742d076b', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:23:06', '2019-08-04 07:23:06', '2020-08-04 13:23:06'),
('6d229e3559e568df086c706a1056f760abc1370abe74033c773490581a042442154afa1260c4b6f0', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:32:12', '2019-08-04 07:32:12', '2020-08-04 13:32:12'),
('6efc0f1fc3843027ea1ea7cd35acf9c74282f0271c31d45a164e7b27025a315d31022efe7bb94aaa', 1, 1, 'Personal Access Token', '[]', 0, '2019-08-08 02:35:26', '2019-08-08 02:35:26', '2020-08-08 08:35:26'),
('7745b763da15a06eaded371330072361b0524c41651cf48bf76fc1b521a475ece78703646e06d3b0', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:29:44', '2019-08-04 07:29:44', '2020-08-04 13:29:44'),
('815b625e239934be293cd34479b0f766bbc1da7cc10d464a2944ddce3a0142e943ae48be018ccbd0', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-22 02:07:47', '2019-07-22 02:07:47', '2020-07-22 08:07:47'),
('8921a4c96a6d674ac002e216f98855c69de2568003f9b4136f6e66f4cb9545442fb3e37e91a27cad', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 06:05:05', '2019-08-04 06:05:05', '2020-08-04 12:05:05'),
('8d8b85720304e2f161a66564cec0ecd50d70e611cc0efbf04e409330086e6009f72a39ce2191f33a', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 06:44:35', '2019-08-04 06:44:35', '2020-08-04 12:44:35'),
('bcaaebdead4c0ef15f3ea6d196fd80749d309e6db8603b235e818cb626a5cea034ff2a55b66e3e1a', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:14:32', '2019-08-04 07:14:32', '2020-08-04 13:14:32'),
('c25417a5c728073ca8ba57058ded43d496a9d2619b434d2a004dd490a64478c08bc3e06ffc1be65d', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-30 01:45:31', '2019-07-30 01:45:31', '2020-07-30 07:45:31'),
('c7423d85b2b5bdc5027cb283be57fa22f5943cae43f60b0ed27e6dd198e46f25e3501b3081ed0777', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-05 05:02:59', '2019-08-05 05:02:59', '2020-08-05 11:02:59'),
('e76f19dbd5c2c4060719fb1006ac56116fd86f7838b4bf74e2c0a0ac9696e724df1e517dbdb357f4', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-15 02:53:40', '2019-07-15 02:53:40', '2020-07-15 08:53:40'),
('ed7c269dd6f9a97750a982f62e0de54749be6950e323cdfef892a1ec93f8ddbacf9fe26e6a42180e', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-13 06:36:45', '2019-07-13 06:36:45', '2020-07-13 12:36:45'),
('f6d1475bc17a27e389000d3df4da5c5004ce7610158b0dd414226700c0f6db48914637b4c76e1948', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:22:01', '2019-08-04 07:22:01', '2020-08-04 13:22:01'),
('f85e4e444fc954430170c41779a4238f84cd6fed905f682795cd4d7b6a291ec5204a10ac0480eb30', 1, 1, 'Personal Access Token', '[]', 1, '2019-07-30 06:38:49', '2019-07-30 06:38:49', '2020-07-30 12:38:49'),
('f8bf983a42c543b99128296e4bc7c2d17a52b5b9ef69670c629b93a653c6a4af27be452e0c331f79', 1, 1, 'Personal Access Token', '[]', 1, '2019-08-04 07:28:55', '2019-08-04 07:28:55', '2020-08-04 13:28:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'eR2y7WUuem28ugHKppFpmss7jPyOHZsMkQwBo1Jj', 'http://localhost', 1, 0, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34'),
(2, NULL, 'Laravel Password Grant Client', 'WLW2Ol0GozbaXEnx1NtXoweYPuKEbjWdviaUgw77', 'http://localhost', 0, 1, 0, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-13 06:17:34', '2019-07-13 06:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `shipping_address` longtext COLLATE utf8_unicode_ci,
  `payment_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'unpaid',
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `grand_total` double(8,2) DEFAULT NULL,
  `coupon_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `code` mediumtext COLLATE utf8_unicode_ci,
  `date` int(20) NOT NULL,
  `viewed` int(1) NOT NULL DEFAULT '0',
  `delivery_viewed` int(1) NOT NULL DEFAULT '1',
  `payment_status_viewed` int(1) DEFAULT '1',
  `commission_calculated` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `guest_id`, `shipping_address`, `payment_type`, `payment_status`, `payment_details`, `grand_total`, `coupon_discount`, `code`, `date`, `viewed`, `delivery_viewed`, `payment_status_viewed`, `commission_calculated`, `created_at`, `updated_at`) VALUES
(6, 8, NULL, '{\"name\":\"Mr. Customer\",\"email\":\"customer@example.com\",\"address\":\"K4aR1zc83d\",\"country\":\"Nepal\",\"city\":\"Gr967GA03D\",\"postal_code\":\"NtM5qOVSGP\",\"phone\":\"3142309476\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 2252.20, 0.00, '20200712-07242272', 1594538662, 0, 0, 0, 0, '2020-07-12 13:24:22', '2020-07-12 13:24:22'),
(7, 12, NULL, '{\"name\":\"admin\",\"email\":\"admin@admin.com\",\"address\":\"Imdol, Gwarko, Lalitpur.\",\"country\":\"Nepal\",\"city\":\"Lalitpur\",\"postal_code\":\"1231\",\"phone\":\"9851275055\",\"checkout_type\":\"logged\"}', 'paypal', 'unpaid', NULL, 4000.00, 0.00, '20200713-07323065', 1594625550, 1, 0, 0, 0, '2020-07-13 13:32:30', '2020-07-13 13:35:23'),
(8, 12, NULL, '{\"name\":\"admin\",\"email\":\"admin@admin.com\",\"address\":\"Imdol, Gwarko, Lalitpur.\",\"country\":\"Nepal\",\"city\":\"Lalitpur\",\"postal_code\":\"1231\",\"phone\":\"9851275055\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'paid', NULL, 4000.00, 0.00, '20200713-07323722', 1594625557, 1, 0, 0, 1, '2020-07-13 13:32:37', '2020-07-13 13:35:36'),
(9, 12, NULL, '{\"name\":\"admin\",\"email\":\"admin@admin.com\",\"address\":\"Imdol, Gwarko, Lalitpur.\",\"country\":\"Nepal\",\"city\":\"Lalitpur\",\"postal_code\":\"1231\",\"phone\":\"9851275055\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200713-07344631', 1594625686, 0, 0, 0, 0, '2020-07-13 13:34:46', '2020-07-13 13:34:46'),
(10, 22, NULL, '{\"name\":\"Sonu\",\"email\":\"sonu@gmail.com\",\"address\":\"Dharam colony\\r\\nPalam Vihar Extension\",\"country\":\"Nepal\",\"city\":\"GURGAON\",\"postal_code\":\"122017\",\"phone\":\"8010198601\",\"checkout_type\":\"logged\"}', 'paypal', 'unpaid', NULL, 17390.00, 0.00, '20200716-17365851', 1594921018, 1, 0, 0, 0, '2020-07-16 23:36:58', '2020-07-17 11:39:12'),
(11, 22, NULL, '{\"name\":\"Sonu\",\"email\":\"sonu@gmail.com\",\"address\":\"Dharam colony\\r\\nPalam Vihar Extension\",\"country\":\"Nepal\",\"city\":\"GURGAON\",\"postal_code\":\"122017\",\"phone\":\"8010198601\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 17390.00, 0.00, '20200716-17375431', 1594921074, 1, 0, 0, 0, '2020-07-16 23:37:54', '2020-07-17 11:39:12'),
(12, 12, NULL, '{\"name\":\"admin\",\"email\":\"admin@admin.com\",\"address\":\"Imdol, Gwarko, Lalitpur.\",\"country\":\"Nepal\",\"city\":\"Lalitpur\",\"postal_code\":\"1231\",\"phone\":\"9851275055\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 33900.00, 0.00, '20200717-04592716', 1594961967, 1, 0, 0, 0, '2020-07-17 10:59:27', '2020-07-17 11:39:12'),
(13, NULL, 228692, '{\"name\":\"kWf636jWg6\",\"email\":\"lc1qd@pvu9.com\",\"address\":\"oUD9AOyHSJ\",\"country\":\"Nepal\",\"city\":\"8VPHL2C2Vv\",\"postal_code\":\"089902\",\"phone\":\"989257\",\"checkout_type\":\"guest\"}', 'paypal', 'unpaid', NULL, 2000.00, 0.00, '20200717-05493072', 1594964970, 1, 0, 0, 0, '2020-07-17 11:49:30', '2020-07-17 19:40:58'),
(14, NULL, 914304, '{\"name\":\"kWf636jWg6\",\"email\":\"lc1qd@pvu9.com\",\"address\":\"oUD9AOyHSJ\",\"country\":\"Nepal\",\"city\":\"8VPHL2C2Vv\",\"postal_code\":\"089902\",\"phone\":\"989257\",\"checkout_type\":\"guest\"}', 'cash_on_delivery', 'unpaid', NULL, 2000.00, 0.00, '20200717-05494584', 1594964985, 1, 0, 0, 0, '2020-07-17 11:49:45', '2020-07-17 19:40:58'),
(15, NULL, 802390, '{\"name\":\"EueLEv4onA\",\"email\":\"BasbthDSHd\",\"address\":\"7y5EZRuZJo\",\"country\":\"Nepal\",\"city\":\"scnEj9GOfD\",\"postal_code\":\"730390\",\"phone\":\"647757\",\"checkout_type\":\"guest\"}', 'cash_on_delivery', 'unpaid', NULL, 3955.00, 0.00, '20200717-06182185', 1594966701, 1, 0, 0, 0, '2020-07-17 12:18:21', '2020-07-17 19:40:58'),
(16, 26, NULL, '{\"name\":\"asdadadad\",\"email\":\"azizdulal.ad@gmail.com\",\"address\":\"qwerty\",\"country\":\"Nepal\",\"city\":\"asdf\",\"postal_code\":\"123123\",\"phone\":\"965465132132\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 33900.00, 0.00, '20200717-12444511', 1594969185, 1, 1, 1, 0, '2020-07-17 18:44:45', '2020-07-17 19:40:58'),
(17, 21, NULL, '{\"name\":\"newseller\",\"email\":\"seller2@gmail.com\",\"address\":\"zbnm,\",\"country\":\"Nepal\",\"city\":\"xsH7MqquAE\",\"postal_code\":\"iNDAB4CiJ2\",\"phone\":\"qliBTP0zjg\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 33900.00, 0.00, '20200717-13330631', 1594972086, 1, 1, 1, 0, '2020-07-17 19:33:06', '2020-07-17 19:41:52'),
(18, NULL, 300490, '{\"name\":\"qwerty\",\"email\":\"azizdulal.ad@gmail.com\",\"address\":\"sasasas\",\"country\":\"Nepal\",\"city\":\"sasasas\",\"postal_code\":\"123456\",\"phone\":\"9846512305\",\"checkout_type\":\"guest\"}', 'cash_on_delivery', 'unpaid', NULL, 3955.00, 0.00, '20200717-14571936', 1594977139, 0, 0, 0, 0, '2020-07-17 20:57:19', '2020-07-17 20:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `variation` longtext COLLATE utf8_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `tax` double(8,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` double(8,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) DEFAULT NULL,
  `payment_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid',
  `delivery_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'pending',
  `shipping_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_point_id` int(11) DEFAULT NULL,
  `product_referral_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `seller_id`, `product_id`, `variation`, `price`, `tax`, `shipping_cost`, `quantity`, `payment_status`, `delivery_status`, `shipping_type`, `pickup_point_id`, `product_referral_code`, `created_at`, `updated_at`) VALUES
(7, 7, 21, 17, 'CornflowerBlue-l', 4000.00, 0.00, 0.00, 2, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-13 13:32:30', '2020-07-13 13:32:30'),
(8, 8, 21, 17, 'CornflowerBlue-l', 4000.00, 0.00, 0.00, 2, 'paid', 'delivered', 'home_delivery', NULL, NULL, '2020-07-13 13:32:37', '2020-07-13 13:35:41'),
(9, 10, 21, 17, 'CornflowerBlue-s', 14000.00, 0.00, 0.00, 7, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-16 23:36:58', '2020-07-16 23:36:58'),
(10, 10, 21, 19, 'Brown-40', 3000.00, 390.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-16 23:36:58', '2020-07-16 23:36:58'),
(11, 11, 21, 17, 'CornflowerBlue-s', 14000.00, 0.00, 0.00, 7, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-16 23:37:54', '2020-07-16 23:37:54'),
(12, 11, 21, 19, 'Brown-40', 3000.00, 390.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-16 23:37:54', '2020-07-16 23:37:54'),
(13, 12, 21, 23, 'Black-32', 30000.00, 3900.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-17 10:59:27', '2020-07-17 10:59:27'),
(14, 13, 21, 17, 'CornflowerBlue-s', 2000.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-17 11:49:30', '2020-07-17 11:49:30'),
(15, 14, 21, 17, 'CornflowerBlue-s', 2000.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-17 11:49:45', '2020-07-17 11:49:45'),
(16, 15, 21, 22, 'Black-small-10ltr', 3500.00, 455.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-17 12:18:21', '2020-07-17 12:18:21'),
(17, 16, 21, 23, 'Black-32', 30000.00, 3900.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-17 18:44:45', '2020-07-17 18:44:45'),
(18, 17, 21, 23, 'Black-32', 30000.00, 3900.00, 0.00, 1, 'unpaid', 'on_delivery', 'home_delivery', NULL, NULL, '2020-07-17 19:33:06', '2020-07-17 19:41:21'),
(19, 18, 21, 22, 'Black-small-10ltr', 3500.00, 455.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, NULL, '2020-07-17 20:57:19', '2020-07-17 20:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` text COLLATE utf8_unicode_ci,
  `meta_description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `meta_title`, `meta_description`, `keywords`, `meta_image`, `created_at`, `updated_at`) VALUES
(1, 'NPR', 'orange', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', 'sonyinc', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', NULL, '2020-07-13 15:29:55', '2020-07-13 15:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `txn_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pickup_points`
--

CREATE TABLE `pickup_points` (
  `id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `pick_up_status` int(1) DEFAULT NULL,
  `cash_on_pickup_status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickup_points`
--

INSERT INTO `pickup_points` (`id`, `staff_id`, `name`, `address`, `phone`, `pick_up_status`, `cash_on_pickup_status`, `created_at`, `updated_at`) VALUES
(6, 1, 'Nep Shopy', 'Imdol, Gwarko, Lalitpur.', '9851275055', 1, NULL, '2020-07-17 19:32:13', '2020-07-17 19:32:13');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

CREATE TABLE `policies` (
  `id` int(11) NOT NULL,
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'support_policy', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisl augue, porttitor et tellus eget, ultrices bibendum lorem. Fusce faucibus augue vitae tincidunt aliquam. Pellentesque vel quam ligula. Donec dictum enim elit, vitae facilisis dui feugiat sit amet. Suspendisse et massa ut tortor convallis sagittis sit amet vel lectus. Aliquam id ligula vel ex hendrerit sagittis. Donec id lectus vitae turpis rutrum volutpat in a ipsum. Nunc tristique justo sit amet sapien mattis, ut lacinia erat finibus.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Sed fringilla ligula quis velit volutpat ullamcorper. Praesent tincidunt nunc at elit pretium venenatis. Curabitur fermentum orci nec ultrices porttitor. Maecenas non felis id diam tincidunt feugiat. Vivamus ut ante et velit congue ullamcorper. Phasellus a lectus eget justo malesuada lacinia vel ac sapien. Nam scelerisque tempus arcu, vel facilisis massa convallis ut. Pellentesque interdum, elit accumsan consectetur tincidunt, lorem purus ultricies odio, at aliquam lectus leo quis sapien. Maecenas nec varius risus. In congue facilisis magna non vehicula.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Donec dignissim libero id purus maximus imperdiet. Mauris magna ex, mattis in euismod finibus, tincidunt in odio. Suspendisse eleifend est ut tortor molestie, id porttitor ipsum consectetur. Integer ullamcorper neque non nunc varius, non accumsan ligula luctus. Quisque varius sem in ligula faucibus rhoncus vel eu elit. Etiam non nibh at tellus ultrices porttitor ut eu est. Sed ut imperdiet massa.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Aliquam sagittis pretium felis, in tincidunt metus ornare interdum. Cras ornare lacus urna, ac molestie erat volutpat ac. Ut eu arcu eget ipsum tristique scelerisque. Etiam ultricies urna at diam tincidunt aliquet. Suspendisse potenti. Nunc suscipit commodo congue. Donec gravida dictum turpis, vel faucibus sapien pretium ut. Cras in erat vel lorem consequat efficitur. Sed ullamcorper tincidunt felis vitae blandit. Proin imperdiet risus ut augue interdum sollicitudin. Sed eu scelerisque lacus, vitae venenatis neque. Aliquam condimentum turpis mi, eget malesuada enim suscipit ut. Integer hendrerit odio urna, et faucibus elit convallis id.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Quisque risus nisi, blandit vel velit egestas, pellentesque auctor arcu. Curabitur ac metus eu leo facilisis bibendum a quis sapien. Nunc varius mattis elit, eget rhoncus lorem pharetra vel. Suspendisse id tellus feugiat, hendrerit magna vel, venenatis nunc. Duis ornare purus a enim facilisis, vitae gravida tellus commodo. Nam varius iaculis nisl, et posuere leo cursus quis. Praesent blandit magna non congue interdum. Etiam vestibulum lectus vitae tempor bibendum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce placerat, risus at volutpat condimentum, augue nisl convallis leo, ac ultricies lectus augue sed mauris.</p><br>', '2020-07-17 07:27:51', '2020-07-17 19:12:51'),
(2, 'return_policy', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisl augue, porttitor et tellus eget, ultrices bibendum lorem. Fusce faucibus augue vitae tincidunt aliquam. Pellentesque vel quam ligula. Donec dictum enim elit, vitae facilisis dui feugiat sit amet. Suspendisse et massa ut tortor convallis sagittis sit amet vel lectus. Aliquam id ligula vel ex hendrerit sagittis. Donec id lectus vitae turpis rutrum volutpat in a ipsum. Nunc tristique justo sit amet sapien mattis, ut lacinia erat finibus.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Sed fringilla ligula quis velit volutpat ullamcorper. Praesent tincidunt nunc at elit pretium venenatis. Curabitur fermentum orci nec ultrices porttitor. Maecenas non felis id diam tincidunt feugiat. Vivamus ut ante et velit congue ullamcorper. Phasellus a lectus eget justo malesuada lacinia vel ac sapien. Nam scelerisque tempus arcu, vel facilisis massa convallis ut. Pellentesque interdum, elit accumsan consectetur tincidunt, lorem purus ultricies odio, at aliquam lectus leo quis sapien. Maecenas nec varius risus. In congue facilisis magna non vehicula.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Donec dignissim libero id purus maximus imperdiet. Mauris magna ex, mattis in euismod finibus, tincidunt in odio. Suspendisse eleifend est ut tortor molestie, id porttitor ipsum consectetur. Integer ullamcorper neque non nunc varius, non accumsan ligula luctus. Quisque varius sem in ligula faucibus rhoncus vel eu elit. Etiam non nibh at tellus ultrices porttitor ut eu est. Sed ut imperdiet massa.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Aliquam sagittis pretium felis, in tincidunt metus ornare interdum. Cras ornare lacus urna, ac molestie erat volutpat ac. Ut eu arcu eget ipsum tristique scelerisque. Etiam ultricies urna at diam tincidunt aliquet. Suspendisse potenti. Nunc suscipit commodo congue. Donec gravida dictum turpis, vel faucibus sapien pretium ut. Cras in erat vel lorem consequat efficitur. Sed ullamcorper tincidunt felis vitae blandit. Proin imperdiet risus ut augue interdum sollicitudin. Sed eu scelerisque lacus, vitae venenatis neque. Aliquam condimentum turpis mi, eget malesuada enim suscipit ut. Integer hendrerit odio urna, et faucibus elit convallis id.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Quisque risus nisi, blandit vel velit egestas, pellentesque auctor arcu. Curabitur ac metus eu leo facilisis bibendum a quis sapien. Nunc varius mattis elit, eget rhoncus lorem pharetra vel. Suspendisse id tellus feugiat, hendrerit magna vel, venenatis nunc. Duis ornare purus a enim facilisis, vitae gravida tellus commodo. Nam varius iaculis nisl, et posuere leo cursus quis. Praesent blandit magna non congue interdum. Etiam vestibulum lectus vitae tempor bibendum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce placerat, risus at volutpat condimentum, augue nisl convallis leo, ac ultricies lectus augue sed mauris.</p><br>', '2020-07-17 07:27:27', '2020-07-17 19:12:27'),
(4, 'seller_policy', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisl augue, porttitor et tellus eget, ultrices bibendum lorem. Fusce faucibus augue vitae tincidunt aliquam. Pellentesque vel quam ligula. Donec dictum enim elit, vitae facilisis dui feugiat sit amet. Suspendisse et massa ut tortor convallis sagittis sit amet vel lectus. Aliquam id ligula vel ex hendrerit sagittis. Donec id lectus vitae turpis rutrum volutpat in a ipsum. Nunc tristique justo sit amet sapien mattis, ut lacinia erat finibus.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Sed fringilla ligula quis velit volutpat ullamcorper. Praesent tincidunt nunc at elit pretium venenatis. Curabitur fermentum orci nec ultrices porttitor. Maecenas non felis id diam tincidunt feugiat. Vivamus ut ante et velit congue ullamcorper. Phasellus a lectus eget justo malesuada lacinia vel ac sapien. Nam scelerisque tempus arcu, vel facilisis massa convallis ut. Pellentesque interdum, elit accumsan consectetur tincidunt, lorem purus ultricies odio, at aliquam lectus leo quis sapien. Maecenas nec varius risus. In congue facilisis magna non vehicula.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Donec dignissim libero id purus maximus imperdiet. Mauris magna ex, mattis in euismod finibus, tincidunt in odio. Suspendisse eleifend est ut tortor molestie, id porttitor ipsum consectetur. Integer ullamcorper neque non nunc varius, non accumsan ligula luctus. Quisque varius sem in ligula faucibus rhoncus vel eu elit. Etiam non nibh at tellus ultrices porttitor ut eu est. Sed ut imperdiet massa.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Aliquam sagittis pretium felis, in tincidunt metus ornare interdum. Cras ornare lacus urna, ac molestie erat volutpat ac. Ut eu arcu eget ipsum tristique scelerisque. Etiam ultricies urna at diam tincidunt aliquet. Suspendisse potenti. Nunc suscipit commodo congue. Donec gravida dictum turpis, vel faucibus sapien pretium ut. Cras in erat vel lorem consequat efficitur. Sed ullamcorper tincidunt felis vitae blandit. Proin imperdiet risus ut augue interdum sollicitudin. Sed eu scelerisque lacus, vitae venenatis neque. Aliquam condimentum turpis mi, eget malesuada enim suscipit ut. Integer hendrerit odio urna, et faucibus elit convallis id.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Quisque risus nisi, blandit vel velit egestas, pellentesque auctor arcu. Curabitur ac metus eu leo facilisis bibendum a quis sapien. Nunc varius mattis elit, eget rhoncus lorem pharetra vel. Suspendisse id tellus feugiat, hendrerit magna vel, venenatis nunc. Duis ornare purus a enim facilisis, vitae gravida tellus commodo. Nam varius iaculis nisl, et posuere leo cursus quis. Praesent blandit magna non congue interdum. Etiam vestibulum lectus vitae tempor bibendum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce placerat, risus at volutpat condimentum, augue nisl convallis leo, ac ultricies lectus augue sed mauris.</p>', '2020-07-17 07:27:02', '2020-07-17 19:12:02'),
(5, 'terms', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisl augue, porttitor et tellus eget, ultrices bibendum lorem. Fusce faucibus augue vitae tincidunt aliquam. Pellentesque vel quam ligula. Donec dictum enim elit, vitae facilisis dui feugiat sit amet. Suspendisse et massa ut tortor convallis sagittis sit amet vel lectus. Aliquam id ligula vel ex hendrerit sagittis. Donec id lectus vitae turpis rutrum volutpat in a ipsum. Nunc tristique justo sit amet sapien mattis, ut lacinia erat finibus.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Sed fringilla ligula quis velit volutpat ullamcorper. Praesent tincidunt nunc at elit pretium venenatis. Curabitur fermentum orci nec ultrices porttitor. Maecenas non felis id diam tincidunt feugiat. Vivamus ut ante et velit congue ullamcorper. Phasellus a lectus eget justo malesuada lacinia vel ac sapien. Nam scelerisque tempus arcu, vel facilisis massa convallis ut. Pellentesque interdum, elit accumsan consectetur tincidunt, lorem purus ultricies odio, at aliquam lectus leo quis sapien. Maecenas nec varius risus. In congue facilisis magna non vehicula.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Donec dignissim libero id purus maximus imperdiet. Mauris magna ex, mattis in euismod finibus, tincidunt in odio. Suspendisse eleifend est ut tortor molestie, id porttitor ipsum consectetur. Integer ullamcorper neque non nunc varius, non accumsan ligula luctus. Quisque varius sem in ligula faucibus rhoncus vel eu elit. Etiam non nibh at tellus ultrices porttitor ut eu est. Sed ut imperdiet massa.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"><br></p>', '2020-07-17 07:28:09', '2020-07-17 19:13:09'),
(6, 'privacy_policy', '<p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisl augue, porttitor et tellus eget, ultrices bibendum lorem. Fusce faucibus augue vitae tincidunt aliquam. Pellentesque vel quam ligula. Donec dictum enim elit, vitae facilisis dui feugiat sit amet. Suspendisse et massa ut tortor convallis sagittis sit amet vel lectus. Aliquam id ligula vel ex hendrerit sagittis. Donec id lectus vitae turpis rutrum volutpat in a ipsum. Nunc tristique justo sit amet sapien mattis, ut lacinia erat finibus.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Sed fringilla ligula quis velit volutpat ullamcorper. Praesent tincidunt nunc at elit pretium venenatis. Curabitur fermentum orci nec ultrices porttitor. Maecenas non felis id diam tincidunt feugiat. Vivamus ut ante et velit congue ullamcorper. Phasellus a lectus eget justo malesuada lacinia vel ac sapien. Nam scelerisque tempus arcu, vel facilisis massa convallis ut. Pellentesque interdum, elit accumsan consectetur tincidunt, lorem purus ultricies odio, at aliquam lectus leo quis sapien. Maecenas nec varius risus. In congue facilisis magna non vehicula.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Donec dignissim libero id purus maximus imperdiet. Mauris magna ex, mattis in euismod finibus, tincidunt in odio. Suspendisse eleifend est ut tortor molestie, id porttitor ipsum consectetur. Integer ullamcorper neque non nunc varius, non accumsan ligula luctus. Quisque varius sem in ligula faucibus rhoncus vel eu elit. Etiam non nibh at tellus ultrices porttitor ut eu est. Sed ut imperdiet massa.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Aliquam sagittis pretium felis, in tincidunt metus ornare interdum. Cras ornare lacus urna, ac molestie erat volutpat ac. Ut eu arcu eget ipsum tristique scelerisque. Etiam ultricies urna at diam tincidunt aliquet. Suspendisse potenti. Nunc suscipit commodo congue. Donec gravida dictum turpis, vel faucibus sapien pretium ut. Cras in erat vel lorem consequat efficitur. Sed ullamcorper tincidunt felis vitae blandit. Proin imperdiet risus ut augue interdum sollicitudin. Sed eu scelerisque lacus, vitae venenatis neque. Aliquam condimentum turpis mi, eget malesuada enim suscipit ut. Integer hendrerit odio urna, et faucibus elit convallis id.</p><p style=\"margin: 0px 0px 15px; padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\">Quisque risus nisi, blandit vel velit egestas, pellentesque auctor arcu. Curabitur ac metus eu leo facilisis bibendum a quis sapien. Nunc varius mattis elit, eget rhoncus lorem pharetra vel. Suspendisse id tellus feugiat, hendrerit magna vel, venenatis nunc. Duis ornare purus a enim facilisis, vitae gravida tellus commodo. Nam varius iaculis nisl, et posuere leo cursus quis. Praesent blandit magna non congue interdum. Etiam vestibulum lectus vitae tempor bibendum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce placerat, risus at volutpat condimentum, augue nisl convallis leo, ac ultricies lectus augue sed mauris.</p><br>', '2020-07-17 07:28:40', '2020-07-17 19:13:40');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `subsubcategory_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `photos` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flash_deal_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_provider` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` mediumtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `unit_price` double(8,2) NOT NULL,
  `purchase_price` double(8,2) NOT NULL,
  `variant_product` int(1) NOT NULL DEFAULT '0',
  `attributes` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `choice_options` mediumtext COLLATE utf8_unicode_ci,
  `colors` mediumtext COLLATE utf8_unicode_ci,
  `variations` text COLLATE utf8_unicode_ci,
  `todays_deal` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '1',
  `featured` int(11) NOT NULL DEFAULT '0',
  `current_stock` int(10) NOT NULL DEFAULT '0',
  `unit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `discount_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` double(8,2) DEFAULT NULL,
  `tax_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_type` varchar(20) CHARACTER SET latin1 DEFAULT 'flat_rate',
  `shipping_cost` double(8,2) DEFAULT '0.00',
  `num_of_sale` int(11) NOT NULL DEFAULT '0',
  `meta_title` mediumtext COLLATE utf8_unicode_ci,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `meta_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `rating` double(8,2) NOT NULL DEFAULT '0.00',
  `barcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `digital` int(1) NOT NULL DEFAULT '0',
  `file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `variant_product`, `attributes`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `rating`, `barcode`, `digital`, `file_name`, `file_path`, `created_at`, `updated_at`) VALUES
(16, 'saree', 'seller', 15, 1, 3, 9, NULL, '[\"uploads\\/products\\/photos\\/BDfOPDfagcY7kyEKAFXdzfQ5IwdiXFDTrndTt2XB.png\"]', 'uploads/products/thumbnail/Wt3Ozq69VaP1qElFLSJWqFAQLa3sy2MLtCbosNjK.png', NULL, NULL, 'youtube', NULL, 'Saree', '<ul><li>Silk Saree</li><li>Womens Saree</li><li>Traditional Dress<br></li></ul>', 2400.00, 1500.00, 1, '[\"2\"]', '[{\"attribute_id\":\"2\",\"values\":[\"Silk\"]}]', '[\"#A52A2A\"]', NULL, 0, 1, 0, 0, '5', 7.00, 'percent', 13.00, 'percent', 'flat_rate', 49.73, 0, NULL, NULL, 'uploads/products/meta/J6Nlo845gfLMSAP6nLR02wYGvBSBPnzCJ4jj91Gh.png', NULL, 'saree-zeX4M', 0.00, NULL, 0, NULL, NULL, '2020-07-12 19:06:21', '2020-07-12 19:22:22'),
(17, 'Boyfriend Jeans Vintage High Waist Jeans For Women Boyfriend Jeans Length Mom Style', 'seller', 21, 1, 3, 7, NULL, '[\"uploads\\/products\\/photos\\/2nxMvfmDRuArMVGd2hPa6ggdrd1qVl9xiAm4riwg.jpeg\"]', 'uploads/products/thumbnail/mwIzKQKxyxKpw43Bk3aRjztVe38m4XRDBMRcTnLX.jpeg', NULL, NULL, 'youtube', NULL, 'pant,jeans', '<p>Lorem ipsum dolor \r\nsit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt \r\nut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 2000.00, 1200.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"s\",\"md\",\"l\"]}]', '[\"#6495ED\"]', NULL, 0, 1, 0, 0, '10', 0.00, 'amount', 0.00, 'amount', NULL, 0.00, 6, 'pant', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, 'Boyfriend-Jeans-Vintage-High-Waist-Jeans-For-Women-Boyfriend-Jeans-Length-Mom-Style-OS6vQ', 0.00, NULL, 0, NULL, NULL, '2020-07-13 13:24:46', '2020-07-17 11:49:45'),
(18, 'SAMSUNG GALAXY M21', 'seller', 21, 3, 8, NULL, 8, '[\"uploads\\/products\\/photos\\/lXq1iKyZolKQMaWVw04TPUWC4oWHfx6fu3ptfzcj.webp\"]', 'uploads/products/thumbnail/QppFJ1ULEmrpRZyAiwEpZggj5KRQDwv9mTm9czfZ.webp', NULL, NULL, 'youtube', NULL, 'mobile,smartphone', '<blockquote>Lorem ipsum dolor \r\nsit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt \r\nut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote>', 20000.00, 18000.00, 1, '[\"4\",\"7\"]', '[{\"attribute_id\":\"4\",\"values\":[\"3gb\",\"6gb\"]},{\"attribute_id\":\"7\",\"values\":[\"6.0inch\"]}]', '[\"#F0F8FF\",\"#000000\"]', NULL, 0, 1, 0, 0, '5', 0.00, 'amount', 13.00, 'percent', NULL, 0.00, 0, 'best smartphones in town', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'uploads/products/meta/Gir76lxEMEpqqRtMovRFKFnzzMJ8ZqKeu5i2nVwL.webp', NULL, 'SAMSUNG-GALAXY-M21-zJg8B', 0.00, NULL, 0, NULL, NULL, '2020-07-13 13:53:07', '2020-07-13 14:24:06'),
(19, 'Remember Shoes Brown Lace Up Lifestyle Boots For Men (Yk-412)', 'seller', 21, 2, 5, 10, NULL, '[\"uploads\\/products\\/photos\\/bsLvw4vqoccpYoBs5fPPqi66U7kOsfawiAMaNTl3.webp\"]', 'uploads/products/thumbnail/Mtp8xFtEUjNznVEsCews0I7W7pZC6N3cVP7uibqz.webp', 'uploads/products/featured/cabanXY3QBurW9DcVLGTODmoFmgutO7W92cJp7RM.webp', 'uploads/products/flash_deal/AdRgMDnyH46pWN8y6Lg8awdX38iF3P2RQv3OKjnt.webp', 'youtube', NULL, 'shoe,formal', '<blockquote>Lorem ipsum dolor \r\nsit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt \r\nut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote><br>', 3000.00, 2200.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"40\",\"41\",\"42\"]}]', '[\"#A52A2A\"]', NULL, 0, 1, 1, 0, '5', 0.00, 'amount', 13.00, 'percent', NULL, 0.00, 2, 'shoe', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL, 'Remember-Shoes-Brown-Lace-Up-Lifestyle-Boots-For-Men-Yk-412-WmicP', 0.00, NULL, 0, NULL, NULL, '2020-07-13 14:04:18', '2020-07-16 23:37:54'),
(20, 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 'seller', 21, 2, 5, 10, NULL, '[\"uploads\\/products\\/photos\\/xDw4vVhKXqRDzjKRmtfVK3ZOuix05N9zIYOWvykA.webp\"]', 'uploads/products/thumbnail/7JyXUDFL83tGpFBsk6PP106NYox6CYw11Y04iQ48.webp', 'uploads/products/featured/BddjNFIikU4rScqoaPdePOf8MhtC1EkdWX4GgWbc.webp', 'uploads/products/flash_deal/4wm1vkQbJ8722v2ksvrrjfDTstmdTr1JlUKIz6Vj.webp', 'youtube', NULL, 'shoe,formal,leather', 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 4000.00, 3000.00, 1, '[\"1\"]', '[{\"attribute_id\":\"1\",\"values\":[\"40\",\"41\",\"42\"]}]', '[\"#A52A2A\"]', NULL, 0, 1, 1, 0, '2', 0.00, 'amount', 13.00, 'percent', NULL, 0.00, 0, 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 'Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)Brown Lace-Up Glossy Formal Shoes For Men (G-917)', 'uploads/products/meta/NuLkkVW20OVBrUQHSKKzEX5Y5j8vrHpCXva9bmhE.webp', NULL, 'Brown-Lace-Up-Glossy-Formal-Shoes-For-Men-G-917-Kd9j0', 0.00, NULL, 0, NULL, NULL, '2020-07-13 14:22:19', '2020-07-13 14:24:08'),
(21, 'Police Zebra Junior Kids T-Shirt', 'seller', 21, 2, 6, 14, NULL, '[\"uploads\\/products\\/photos\\/RjFuyW05m70y8ZBtQB5k1MfYt0yxl7DXhTJv5Bt1.webp\"]', 'uploads/products/thumbnail/sagH1QBo9yT5rHX8OyfXiDVGMKS2d4FpAWLIA8g8.webp', 'uploads/products/featured/QnOgsvJpYw3cG0U4H4H9vAiq5FvMcgHzSch1LDEd.webp', 'uploads/products/flash_deal/lsr2mV6O0jqalP4uQKtAFGWeLj4Kl0K18s9jSAQa.webp', 'youtube', NULL, 'TSHIRT,KIDS', 'Police Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-Shirt', 1000.00, 700.00, 1, '[]', '[]', '[\"#8B4513\"]', NULL, 0, 1, 0, 0, '20', 0.00, 'amount', 0.00, 'amount', NULL, 0.00, 0, 'Police Zebra Junior Kids T-Shirt', 'Police Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-ShirtPolice Zebra Junior Kids T-Shirt', NULL, NULL, 'Police-Zebra-Junior-Kids-T-Shirt-bCb8g', 0.00, NULL, 0, NULL, NULL, '2020-07-13 14:32:42', '2020-07-13 14:32:42'),
(22, 'Anti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, Black', 'seller', 21, 2, 6, NULL, NULL, '[\"uploads\\/products\\/photos\\/rHVMaxzux7PxJkaTJPqm1mOZ15q9YslxgMife6qC.webp\"]', 'uploads/products/thumbnail/AeAqWKIuLE3TOvG9vI31bxLE4TRPM6YkzCrkIf11.webp', 'uploads/products/featured/OGlepzJ0KYGfwuWVpH9JYSN6c16tmxaFPCaT47II.webp', 'uploads/products/flash_deal/LcOwD9VkH3bweVNrDRh2n8WxvBwMqU7aAvW0EZh5.webp', 'youtube', NULL, 'bag,antitheft', 'Anti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, BlackAnti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, BlackAnti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, BlackAnti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, Black', 3500.00, 2000.00, 1, '[\"1\",\"8\"]', '[{\"attribute_id\":\"1\",\"values\":[\"small\",\"large\"]},{\"attribute_id\":\"8\",\"values\":[\"10ltr\",\"20ltr\"]}]', '[\"#000000\",\"#0000FF\",\"#D3D3D3\"]', NULL, 0, 1, 1, 0, '20', 0.00, 'percent', 13.00, 'percent', NULL, 0.00, 2, 'Anti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, Black', 'Anti Theft Laptop Backpack - Arctic Hunter Waterproof Backpack with USB Charging Port for 15.6\'\' Laptop, Black', NULL, NULL, 'Anti-Theft-Laptop-Backpack---Arctic-Hunter-Waterproof-Backpack-with-USB-Charging-Port-for-156-Laptop-Black-RjQlq', 0.00, NULL, 0, NULL, NULL, '2020-07-13 14:36:24', '2020-07-17 20:57:19'),
(23, 'SONY Bravia Klv-40W652D 40 Led Smart Tv', 'seller', 21, 4, 10, 18, 9, '[\"uploads\\/products\\/photos\\/KHlDmc1exim8cVov25fwCwlpVPqxp721DJJ81451.webp\",\"uploads\\/products\\/photos\\/fGg3UUGIDwT36Gm3hnJl8ieoc8CsLtImQtBCGgDh.webp\"]', 'uploads/products/thumbnail/ZJvUl6CBhFZbZDC08bAeeYNqN5IunuSLpcEJfnKe.webp', 'uploads/products/featured/Ysw0F8mo7KBRKQYw7IsFhaxyguQDxwPKBHVCF09b.webp', 'uploads/products/flash_deal/HPjVqMaxcbEnjviWCdfQW8258ufXT2r1uyOwuWxv.webp', 'youtube', NULL, 'tv,led,smart', 'SONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart Tv', 30000.00, 20000.00, 1, '[\"7\"]', '[{\"attribute_id\":\"7\",\"values\":[\"32\",\"40\",\"50\"]}]', '[\"#000000\"]', NULL, 0, 1, 1, 0, '20', 0.00, 'amount', 13.00, 'percent', NULL, 0.00, 3, 'SONY Bravia Klv-40W652D 40 Led Smart Tv', 'SONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart TvSONY Bravia Klv-40W652D 40 Led Smart Tv', NULL, NULL, 'SONY-Bravia-Klv-40W652D-40-Led-Smart-Tv-P4KI7', 0.00, NULL, 0, NULL, NULL, '2020-07-13 15:37:42', '2020-07-17 19:33:06'),
(24, 'Samsung Black 80 cm (32 Inches) LED TV- UA32N4003ARXXL', 'seller', 21, 4, 10, 18, 8, '[\"uploads\\/products\\/photos\\/0DnfFK8IPx82ZZ0qyBYdPV1at0naYZiKJq5PNsof.webp\",\"uploads\\/products\\/photos\\/KHjGxzVeaccxvrOPpQmiqOfLGv489ByLCNYqKfof.webp\"]', 'uploads/products/thumbnail/gukKQbLC9Znw7iA9yr5xA3DhcrN1As7BcnrRRXsN.webp', 'uploads/products/featured/p7fei8jkT6olSpDJWQS4CXjJ1lc7aYtP09YSvGPi.webp', 'uploads/products/flash_deal/l8sRvgHYVYLGbNGsa8vXLQ3FBWRigIrlnhEBoPf0.webp', 'youtube', NULL, 'samsung,ledtv', '<blockquote>Lorem ipsum dolor \r\nsit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt \r\nut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud \r\nexercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum \r\ndolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non \r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote><br>', 300000.00, 24999.98, 1, '[\"7\"]', '[{\"attribute_id\":\"7\",\"values\":[\"30\",\"50\"]}]', '[]', NULL, 0, 1, 1, 0, '20', 0.00, 'amount', 13.00, 'percent', NULL, 0.00, 0, 'samsung', 'samsungsamsungsamsungsamsungsamsungsamsungsamsungsamsungsamsungsamsungsamsungsamsungsamsungsamsung', NULL, NULL, 'Samsung-Black-80-cm-32-Inches-LED-TV--UA32N4003ARXXL-E7UR8', 0.00, NULL, 0, NULL, NULL, '2020-07-13 15:38:52', '2020-07-16 22:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `product_stocks`
--

CREATE TABLE `product_stocks` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `variant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` double(10,2) NOT NULL DEFAULT '0.00',
  `qty` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_stocks`
--

INSERT INTO `product_stocks` (`id`, `product_id`, `variant`, `sku`, `price`, `qty`, `created_at`, `updated_at`) VALUES
(1, 15, 'Amethyst-l', 'Y-Amethyst-l', 1000.00, 6, '2020-07-12 11:03:04', '2020-07-12 13:24:22'),
(2, 15, 'Amethyst-m', 'Y-Amethyst-m', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(3, 15, 'Amethyst-s', 'Y-Amethyst-s', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(4, 15, 'AntiqueWhite-l', 'Y-AntiqueWhite-l', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(5, 15, 'AntiqueWhite-m', 'Y-AntiqueWhite-m', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(6, 15, 'AntiqueWhite-s', 'Y-AntiqueWhite-s', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(7, 15, 'Aquamarine-l', 'Y-Aquamarine-l', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(8, 15, 'Aquamarine-m', 'Y-Aquamarine-m', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(9, 15, 'Aquamarine-s', 'Y-Aquamarine-s', 1000.00, 10, '2020-07-12 11:03:04', '2020-07-12 11:03:04'),
(10, 16, 'Brown-Silk', 's-Brown-Silk', 2400.00, 0, '2020-07-12 19:06:21', '2020-07-12 19:06:21'),
(11, 17, 'CornflowerBlue-s', 'BJVHWJFWBJLMS-CornflowerBlue-s', 2000.00, 2, '2020-07-13 13:24:46', '2020-07-17 11:49:45'),
(12, 17, 'CornflowerBlue-md', 'BJVHWJFWBJLMS-CornflowerBlue-md', 2000.00, 10, '2020-07-13 13:24:46', '2020-07-13 13:24:46'),
(13, 17, 'CornflowerBlue-l', 'BJVHWJFWBJLMS-CornflowerBlue-l', 2000.00, -2, '2020-07-13 13:24:46', '2020-07-17 11:40:48'),
(14, 18, 'AliceBlue-3gb-6.0inch', 'SGM-AliceBlue-3gb-6.0inch', 20000.00, 5, '2020-07-13 13:53:07', '2020-07-13 13:53:07'),
(15, 18, 'AliceBlue-6gb-6.0inch', 'SGM-AliceBlue-6gb-6.0inch', 20000.00, 5, '2020-07-13 13:53:07', '2020-07-13 13:53:07'),
(16, 18, 'Black-3gb-6.0inch', 'SGM-Black-3gb-6.0inch', 20000.00, 5, '2020-07-13 13:53:07', '2020-07-13 13:53:07'),
(17, 18, 'Black-6gb-6.0inch', 'SGM-Black-6gb-6.0inch', 20000.00, 5, '2020-07-13 13:53:07', '2020-07-13 13:53:07'),
(18, 19, 'Brown-40', 'RSBLULBFM(-Brown-40', 3000.00, 8, '2020-07-13 14:04:18', '2020-07-16 23:37:54'),
(19, 19, 'Brown-41', 'RSBLULBFM(-Brown-41', 3500.00, 10, '2020-07-13 14:04:18', '2020-07-13 14:04:18'),
(20, 19, 'Brown-42', 'RSBLULBFM(-Brown-42', 3800.00, 10, '2020-07-13 14:04:18', '2020-07-13 14:04:18'),
(21, 20, 'Brown-40', 'BLGFSFM(-Brown-40', 4000.00, 1, '2020-07-13 14:22:19', '2020-07-13 14:22:19'),
(22, 20, 'Brown-41', 'BLGFSFM(-Brown-41', 4000.00, 1, '2020-07-13 14:22:19', '2020-07-13 14:22:19'),
(23, 20, 'Brown-42', 'BLGFSFM(-Brown-42', 4000.00, 1, '2020-07-13 14:22:19', '2020-07-13 14:22:19'),
(24, 21, 'SaddleBrown', 'PZJKT-SaddleBrown', 1000.00, 20, '2020-07-13 14:32:42', '2020-07-13 14:32:42'),
(25, 22, 'Black-small-10ltr', 'ATLB-AHWBwUCPf1LB-Black-small-10ltr', 3500.00, 8, '2020-07-13 14:36:24', '2020-07-17 20:57:19'),
(26, 22, 'Black-small-20ltr', 'ATLB-AHWBwUCPf1LB-Black-small-20ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(27, 22, 'Black-large-10ltr', 'ATLB-AHWBwUCPf1LB-Black-large-10ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(28, 22, 'Black-large-20ltr', 'ATLB-AHWBwUCPf1LB-Black-large-20ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(29, 22, 'Blue-small-10ltr', 'ATLB-AHWBwUCPf1LB-Blue-small-10ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(30, 22, 'Blue-small-20ltr', 'ATLB-AHWBwUCPf1LB-Blue-small-20ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(31, 22, 'Blue-large-10ltr', 'ATLB-AHWBwUCPf1LB-Blue-large-10ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(32, 22, 'Blue-large-20ltr', 'ATLB-AHWBwUCPf1LB-Blue-large-20ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(33, 22, 'LightGrey-small-10ltr', 'ATLB-AHWBwUCPf1LB-LightGrey-small-10ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(34, 22, 'LightGrey-small-20ltr', 'ATLB-AHWBwUCPf1LB-LightGrey-small-20ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(35, 22, 'LightGrey-large-10ltr', 'ATLB-AHWBwUCPf1LB-LightGrey-large-10ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(36, 22, 'LightGrey-large-20ltr', 'ATLB-AHWBwUCPf1LB-LightGrey-large-20ltr', 3500.00, 10, '2020-07-13 14:36:24', '2020-07-13 14:36:24'),
(37, 23, 'Black-32', 'SBK4LST-Black-32', 30000.00, 2, '2020-07-13 15:37:42', '2020-07-17 19:33:06'),
(38, 23, 'Black-40', 'SBK4LST-Black-40', 60000.00, 10, '2020-07-13 15:37:42', '2020-07-13 15:37:42'),
(39, 23, 'Black-50', 'SBK4LST-Black-50', 80000.00, 5, '2020-07-13 15:37:42', '2020-07-13 15:37:42'),
(40, 24, '30', 'SB8c(ILTU-30', 30000.00, 10, '2020-07-13 15:41:37', '2020-07-13 15:41:37'),
(41, 24, '50', 'SB8c(ILTU-50', 602000.00, 10, '2020-07-13 15:41:37', '2020-07-13 15:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Manager', '[\"1\",\"2\",\"4\",\"8\"]', '2018-10-10 04:39:47', '2020-07-12 20:09:57'),
(2, 'Accountant', '[\"2\",\"3\",\"4\",\"5\"]', '2018-10-10 04:52:09', '2020-07-12 20:09:02'),
(3, 'Admin', '[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\"]', '2020-07-12 16:38:58', '2020-07-12 16:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `searches`
--

CREATE TABLE `searches` (
  `id` int(11) NOT NULL,
  `query` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `searches`
--

INSERT INTO `searches` (`id`, `query`, `count`, `created_at`, `updated_at`) VALUES
(2, 'dcs', 1, '2020-03-08 00:29:09', '2020-03-08 00:29:09'),
(3, 'das', 3, '2020-03-08 00:29:15', '2020-03-08 00:29:50'),
(4, 'led', 1, '2020-07-13 15:43:38', '2020-07-13 15:43:38'),
(5, 'bag', 1, '2020-07-13 15:43:45', '2020-07-13 15:43:45'),
(6, 'shoes', 1, '2020-07-13 15:43:50', '2020-07-13 15:43:50'),
(7, 'sony', 5, '2020-07-17 18:56:30', '2020-07-17 18:57:19');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `verification_status` int(1) NOT NULL DEFAULT '0',
  `verification_info` longtext COLLATE utf8_unicode_ci,
  `cash_on_delivery_status` int(1) NOT NULL DEFAULT '0',
  `admin_to_pay` double(8,2) NOT NULL DEFAULT '0.00',
  `bank_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_acc_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_acc_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_routing_no` int(50) DEFAULT NULL,
  `bank_payment_status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `user_id`, `verification_status`, `verification_info`, `cash_on_delivery_status`, `admin_to_pay`, `bank_name`, `bank_acc_name`, `bank_acc_no`, `bank_routing_no`, `bank_payment_status`, `created_at`, `updated_at`) VALUES
(3, 15, 1, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-07-12 18:48:01', '2020-07-12 18:49:41'),
(6, 20, 1, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-07-13 05:17:53', '2020-07-13 05:17:53'),
(7, 21, 1, NULL, 0, -200.00, NULL, NULL, NULL, NULL, 0, '2020-07-13 12:28:05', '2020-07-13 14:49:47'),
(8, 26, 1, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-07-17 12:47:02', '2020-07-17 19:26:29'),
(9, 27, 1, NULL, 0, 0.00, NULL, NULL, NULL, NULL, 0, '2020-07-17 22:59:16', '2020-07-17 22:59:51');

-- --------------------------------------------------------

--
-- Table structure for table `seller_withdraw_requests`
--

CREATE TABLE `seller_withdraw_requests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `message` longtext,
  `status` int(1) DEFAULT NULL,
  `viewed` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `seo_settings`
--

CREATE TABLE `seo_settings` (
  `id` int(11) NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisit` int(11) NOT NULL,
  `sitemap_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo_settings`
--

INSERT INTO `seo_settings` (`id`, `keyword`, `author`, `revisit`, `sitemap_link`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Online Shopping,Dress, Saree', 'Nepshopy', 11, 'https://www.nepshopy.com', 'Nepshopy understands its shoppers\' needs and caters to them with choice of apparel, accessories, cosmetics and footwear from leading  international brands.', '2020-07-13 09:26:46', '2020-07-13 15:26:46');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sliders` longtext COLLATE utf8_unicode_ci,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `pick_up_point_id` text COLLATE utf8_unicode_ci,
  `shipping_cost` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `user_id`, `name`, `logo`, `sliders`, `address`, `facebook`, `google`, `twitter`, `youtube`, `slug`, `meta_title`, `meta_description`, `pick_up_point_id`, `shipping_cost`, `created_at`, `updated_at`) VALUES
(3, 15, 'Nepshopy Mall', NULL, NULL, 'Imdol, Gwarko, Lalitpur.', NULL, NULL, NULL, NULL, 'Nepshopy-Mall-3', 'Women\'s Fashion', 'Clothing', '[]', 0.00, '2020-07-12 18:48:01', '2020-07-12 18:49:09'),
(6, 21, 'New Shop', 'uploads/shop/logo/xeRjNOyR6FosdwwzkxQg1dGCR354l7GapOBMu2Hi.png', NULL, 'ktm', NULL, NULL, NULL, NULL, 'New-Shop-6', 'Automotive & Motorbike', 'Automotive & MotorbikeAutomotive & MotorbikeAutomotive & Motorbike', '[]', 0.00, '2020-07-13 12:28:05', '2020-07-13 14:58:43'),
(7, 26, 'asdadadad', NULL, '[\"uploads\\/shop\\/sliders\\/Xbpo2plIexeZXoyBedm93Rjw4OCiU6eD5HSc3G75.jpeg\"]', 'asasacaxzaxasdas', NULL, NULL, NULL, NULL, 'asdadadad-7', 'your shop', 'all at this shop', '[]', 50.00, '2020-07-17 12:47:02', '2020-07-17 12:48:11'),
(8, 27, 'designer Shop', NULL, NULL, 'bhaktapur', NULL, NULL, NULL, NULL, 'designer-Shop-', NULL, NULL, NULL, 0.00, '2020-07-17 22:59:16', '2020-07-17 22:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `photo`, `published`, `link`, `created_at`, `updated_at`) VALUES
(7, 'uploads/sliders/slider-image.jpg', 0, NULL, '2019-03-12 05:58:05', '2020-07-12 16:59:56'),
(8, 'uploads/sliders/slider-image.jpg', 0, NULL, '2019-03-12 05:58:12', '2020-07-12 16:59:56'),
(10, 'uploads/sliders/66IQ7saPUsF5mtsfu0O0p4FR7x0SXA7JdXJO8skN.png', 1, 'http://nepshopy.com/', '2020-07-13 13:26:12', '2020-07-13 13:26:12'),
(11, 'uploads/sliders/jEBPpmuV9QzZZ5VVqjARIv8I6GZdTSuSNS6YMuXV.jpeg', 1, 'seller.nepshopy.com', '2020-07-13 14:10:45', '2020-07-13 14:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 13, 3, '2020-07-12 16:37:52', '2020-07-12 16:39:12'),
(2, 22, 3, '2020-07-16 22:08:31', '2020-07-16 22:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Accessories', 1, 'Accessoriesq-cqnSt', 'accessories', 'All other accessories', '2019-03-12 06:13:24', '2020-07-12 16:21:08'),
(2, 'Traditional clothing', 1, 'Traditional-clothing-4W98I', 'traditionalclothing', 'all traditional clothing', '2019-03-12 06:13:44', '2020-07-12 16:20:22'),
(3, 'clothing', 1, 'clothing-hOsSo', 'clothing', 'all clothing items', '2019-03-12 06:13:59', '2020-07-12 16:18:55'),
(4, 'Desktop', 3, 'Desktop-FeBmQ', 'desktops', 'all desktop items', '2019-03-12 06:18:25', '2020-07-12 16:18:06'),
(5, 'Shoes', 2, 'Shoes-lNQqv', 'shoes', 'all shoes items', '2019-03-12 06:18:38', '2020-07-12 16:17:38'),
(6, 'Clothing', 2, 'Clothing-dseah', 'clothing', 'all clothing items', '2019-03-12 06:18:51', '2020-07-12 16:17:12'),
(7, 'Laptops', 3, 'Laptops-AEAaW', 'laptop', 'all Laptop items', '2019-03-12 06:19:05', '2020-07-12 16:16:43'),
(8, 'Mobiles & Tablets', 3, 'Mobiles--Tablets-SmEJc', 'mobiles', 'all moibile items', '2019-03-12 06:19:13', '2020-07-12 16:16:11'),
(9, 'Kurthas', 1, 'Kurthas-dQ6kM', 'kurthas', 'all kurtha items', '2019-03-12 06:19:22', '2020-07-12 16:15:26'),
(10, 'televisions', 4, 'televisions-4aZMU', 'tv', 'tvs', '2020-07-13 15:33:51', '2020-07-13 15:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

CREATE TABLE `sub_sub_categories` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `name`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Demo sub sub category', 'Demo-sub-sub-category', 'Demo sub sub category', NULL, '2019-03-12 06:19:49', '2019-08-06 06:07:19'),
(2, 1, 'Demo sub sub category 2', 'Demo-sub-sub-category-2', 'Demo sub sub category 2', NULL, '2019-03-12 06:20:23', '2019-08-06 06:07:19'),
(3, 1, 'Sandals', 'Sandals-rrXbH', 'sandals', 'all sandals items', '2019-03-12 06:20:43', '2020-07-12 16:46:26'),
(4, 3, 'Woolen Clothing', 'Woolen-Clothing-UpC0o', 'Woolen Clothing', 'all Woolen Clothingitems', '2019-03-12 06:21:28', '2020-07-12 16:46:48'),
(5, 3, 'plazo', 'plazo-kvvws', 'plazo', 'allplazo items', '2019-03-12 06:21:40', '2020-07-12 16:45:12'),
(6, 3, '3 piece', '3-piece-hAhuQ', '3piece', 'all 3 piece items', '2019-03-12 06:21:56', '2020-07-12 16:44:42'),
(7, 3, 'Pant', 'Pant-pzVuj', 'pant', 'all pants', '2019-03-12 06:23:31', '2020-07-12 16:43:51'),
(8, 3, 'Gown', 'Gown-doW2x', 'gown', 'all gown items', '2019-03-12 06:23:48', '2020-07-12 16:43:15'),
(9, 3, 'saree', 'Demo-sub-sub-category-3', 'saree', NULL, '2019-03-12 06:24:01', '2020-07-12 16:42:46'),
(10, 5, 'formal shoes', 'formal-shoes-1N7co', 'formal', 'All formal shoes', '2019-03-12 06:24:37', '2020-07-12 16:49:23'),
(11, 4, 'Normal / Office', 'Normal--Office-E2CzO', 'normal', 'All normal and office use desktops', '2019-03-12 06:25:14', '2020-07-12 16:48:51'),
(12, 4, 'Gaming', 'Gaming-GcBdE', 'gamingh', 'all gaming desktops', '2019-03-12 06:25:25', '2020-07-12 16:48:05'),
(13, 5, 'sneakers', 'sneakers-oeIIc', 'sneakers', 'all sneakers items', '2019-03-12 06:25:58', '2020-07-12 16:47:30'),
(14, 6, 'casual wear', 'casual-wear-XoC6b', 'casualwear', 'all casual wears', '2019-03-12 06:26:16', '2020-07-12 16:35:00'),
(15, 7, 'gaming', 'gaming-leIOt', 'gaming', 'all gaming laptops', '2019-03-12 06:27:17', '2020-07-12 16:34:22'),
(16, 8, 'Tablets', 'Tablets-iBApl', 'tablet', 'all tablets', '2019-03-12 06:27:29', '2020-07-12 16:33:43'),
(17, 4, 'Gaming', 'Gaming-OM7SZ', 'gaming', 'all gaming desktops', '2019-03-12 06:27:41', '2020-07-12 16:25:11'),
(18, 10, 'led', 'led-scpCm', 'led tvs', 'led tvs', '2020-07-13 15:34:14', '2020-07-13 15:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `code` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `client_viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `code`, `user_id`, `subject`, `details`, `files`, `status`, `viewed`, `client_viewed`, `created_at`, `updated_at`) VALUES
(8, 10000029, 21, 'test problem', 'sadsatgtreagreh', NULL, 'solved', 1, 1, '2020-07-13 10:06:30', '2020-07-13 16:06:30'),
(9, 1000003029, 27, '<script type=\"text/javascript\"> 	function confirmation(evt){ 		let result = confirm(\"Are you sure to Delete?\"); 		if(! result){ 			evt.stopPropagation(); 			evt.preventDefault();	 		} 	} </script>', '<script>\r\n	function confirmation(evt){\r\n		let result = confirm(\"Are you sure to Delete?\");\r\n		if(! result){\r\n			evt.stopPropagation();\r\n			evt.preventDefault();	\r\n		}\r\n	}\r\n</script><br>', NULL, 'pending', 0, 0, '2020-07-17 21:55:29', '2020-07-17 21:55:29'),
(10, 2147483647, 27, '<script type=\"text/javascript\"> 	function confirmation(evt){ 		let result = confirm(\"Are you sure to Delete?\"); 		if(! result){ 			evt.stopPropagation(); 			evt.preventDefault();	 		} 	} </script>', '<script>\r\n	function confirmation(evt){\r\n		let result = confirm(\"Are you sure to Delete?\");\r\n		if(! result){\r\n			evt.stopPropagation();\r\n			evt.preventDefault();	\r\n		}\r\n	}\r\n</script><br>', NULL, 'pending', 0, 0, '2020-07-17 21:55:31', '2020-07-17 21:55:31'),
(11, 2147483647, 27, '<script type=\"text/javascript\"> 	function confirmation(evt){ 		let result = confirm(\"Are you sure to Delete?\"); 		if(! result){ 			evt.stopPropagation(); 			evt.preventDefault();	 		} 	} </script>', '<script>\r\n	function confirmation(evt){\r\n		let result = confirm(\"Are you sure to Delete?\");\r\n		if(! result){\r\n			evt.stopPropagation();\r\n			evt.preventDefault();	\r\n		}\r\n	}\r\n</script><br>', NULL, 'pending', 0, 0, '2020-07-17 21:55:32', '2020-07-17 21:55:32'),
(12, 2147483647, 27, '<script type=\"text/javascript\"> 	function confirmation(evt){ 		let result = confirm(\"Are you sure to Delete?\"); 		if(! result){ 			evt.stopPropagation(); 			evt.preventDefault();	 		} 	} </script>', '<script>\r\n	function confirmation(evt){\r\n		let result = confirm(\"Are you sure to Delete?\");\r\n		if(! result){\r\n			evt.stopPropagation();\r\n			evt.preventDefault();	\r\n		}\r\n	}\r\n</script><br>', NULL, 'pending', 1, 0, '2020-07-17 10:10:55', '2020-07-17 21:55:55');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_replies`
--

CREATE TABLE `ticket_replies` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply` longtext COLLATE utf8_unicode_ci NOT NULL,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket_replies`
--

INSERT INTO `ticket_replies` (`id`, `ticket_id`, `user_id`, `reply`, `files`, `created_at`, `updated_at`) VALUES
(8, 8, 12, 'qwertyuiop12345', NULL, '2020-07-13 15:24:03', '2020-07-13 15:24:03'),
(9, 8, 12, 'asasasas', NULL, '2020-07-13 15:24:14', '2020-07-13 15:24:14'),
(10, 8, 21, 'asdfghjklll;', NULL, '2020-07-13 15:25:19', '2020-07-13 15:25:19'),
(11, 8, 21, 'asdfghjklll;', NULL, '2020-07-13 15:25:45', '2020-07-13 15:25:45'),
(12, 8, 21, 'ljkshkagskajbs', NULL, '2020-07-13 16:05:21', '2020-07-13 16:05:21'),
(13, 8, 12, 'ok thank you', NULL, '2020-07-13 16:06:16', '2020-07-13 16:06:16'),
(14, 8, 12, 'ok thank you', NULL, '2020-07-13 16:06:16', '2020-07-13 16:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `referred_by` int(11) DEFAULT NULL,
  `provider_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'customer',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_original` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `referral_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_package_id` int(11) DEFAULT NULL,
  `remaining_uploads` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `referred_by`, `provider_id`, `user_type`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `avatar`, `avatar_original`, `address`, `country`, `city`, `postal_code`, `phone`, `balance`, `referral_code`, `customer_package_id`, `remaining_uploads`, `created_at`, `updated_at`) VALUES
(8, NULL, NULL, 'customer', 'Mr. Customer', 'customer@example.com', '2018-12-11 18:00:00', '$2y$10$eUKRlkmm2TAug75cfGQ4i.WoUbcJ2uVPqUlVkox.cv4CCyGEIMQEm', 'zileondgipAvHWatKgj3dVXfGxYDpjnMHVpXAj6liQkTffYHuMy8ja5p1T63', 'https://lh3.googleusercontent.com/-7OnRtLyua5Q/AAAAAAAAAAI/AAAAAAAADRk/VqWKMl4f8CI/photo.jpg?sz=50', 'uploads/ucQhvfz4EQXNeTbN8Eif0Cpq5LnOwvg8t7qKNKVs.jpeg', 'Demo address', 'US', 'Demo city', '1234', NULL, 0.00, '8zJTyXTlTT', NULL, NULL, '2018-10-07 04:42:57', '2020-03-03 04:26:11'),
(12, NULL, NULL, 'admin', 'admin', 'admin@admin.com', '2020-07-12 10:07:23', '$2y$10$9UfJw/fYyLLV0lFqRHhHe.xsFwbD5xoTNF5se8aBZtrotbfH6zt76', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-12 10:54:23', '2020-07-12 10:54:23'),
(13, NULL, NULL, 'staff', 'Sunil Singh', 'nepshopy@gmail.com', '2020-07-13 17:56:31', '$2y$10$WNjw80fqZx28tYl8R8TM0e/WhRW1sgtdBhzgq5.D0XTEPil6KnYD.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9841481154', 0.00, NULL, NULL, 0, '2020-07-12 16:37:52', '2020-07-12 16:41:46'),
(15, NULL, NULL, 'seller', 'Nepshopy Mall', 'shop.nepshopy@gmail.com', '2020-07-12 18:07:01', '$2y$10$2o/wFih3pJqlwsk3nT9lt.KUhyHPydsm2Bk/I.43x4tUoqqBxM5Fa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-12 18:48:01', '2020-07-12 18:48:01'),
(19, NULL, NULL, 'customer', 'Anita', 'anita.dreamearth@gmail.com', '2020-07-01 17:56:24', '$2y$10$6i.h0YuT7zFQ1w/hTsiKJO351AIh7fg1FkChRT8v/O7K6bP77rQma', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-13 11:12:32', '2020-07-13 11:12:32'),
(20, NULL, NULL, 'seller', 'Nep Shopy', 'seller1@gmail.com', '2020-07-13 17:55:48', '$2y$10$1IjAvLDo7nm7Wbgi94zdCOgpoNo5PUDjZgCbt5b4rkawRrcatQpRO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-13 12:00:47', '2020-07-13 15:02:56'),
(21, NULL, NULL, 'seller', 'newseller', 'seller2@gmail.com', '2020-07-01 18:58:38', '$2y$10$eX.iNdgukWcyGHgvigkDQ.aPbC8XIYmZDjQ4aKnu8NmSxOJC4tuqO', NULL, NULL, 'uploads/PKcHcAi8JGfrHE6U1WkR7Gp5wrkoRRVPVOhhorNK.webp', NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-13 12:28:05', '2020-07-13 14:57:02'),
(22, NULL, NULL, 'staff', 'Sonu', 'sonu@gmail.com', NULL, '$2y$10$UPSahRMgBQq5Bv0nMjOO7O8iylgBPPKw2jBeU9U4JV9UaTauzmunC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1111111111', 0.00, NULL, NULL, 0, '2020-07-16 22:08:31', '2020-07-16 22:08:31'),
(23, NULL, NULL, 'customer', 'Vimal Kumar', 'vimal@gmail.com', NULL, '$2y$10$KxyPKvmKjOIf0Dx9FJzTuOmsqPc9KJDqHKW4lXGp/Gp81oI2pr60W', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-16 23:50:39', '2020-07-16 23:50:39'),
(24, NULL, NULL, 'customer', 'Vimal Kumar', 'sv020293@gmail.com', NULL, '$2y$10$wtdA3q/xbVSA/.C4MmVeye5wnmsq2Jjgg1fRf0MOEN6HJbDVLnUtW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-16 23:54:57', '2020-07-16 23:54:57'),
(27, NULL, NULL, 'seller', 'designer World', 'azizdulal.ad@gmail.com', '2020-07-17 23:06:13', '$2y$10$N6krZxsWEt3hNL2gRt2NgOq.IQvTPddB7P6riqpRM0L2V/som0xJy', NULL, NULL, 'uploads/users/ysMwJ2Jsa8iueHYOcF41rx9gesbBpQIgz3hbOJKd.webp', NULL, NULL, NULL, NULL, NULL, 0.00, NULL, NULL, 0, '2020-07-17 20:51:25', '2020-07-17 23:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

CREATE TABLE `wishlists` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wishlists`
--

INSERT INTO `wishlists` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 21, 20, '2020-07-17 19:45:55', '2020-07-17 19:45:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_settings`
--
ALTER TABLE `business_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conversations`
--
ALTER TABLE `conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_usages`
--
ALTER TABLE `coupon_usages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_packages`
--
ALTER TABLE `customer_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_products`
--
ALTER TABLE `customer_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flash_deals`
--
ALTER TABLE `flash_deals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flash_deal_products`
--
ALTER TABLE `flash_deal_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_categories`
--
ALTER TABLE `home_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pickup_points`
--
ALTER TABLE `pickup_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `policies`
--
ALTER TABLE `policies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searches`
--
ALTER TABLE `searches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_withdraw_requests`
--
ALTER TABLE `seller_withdraw_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_settings`
--
ALTER TABLE `seo_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category_id` (`category_id`);

--
-- Indexes for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_category_id` (`sub_category_id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlists`
--
ALTER TABLE `wishlists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addons`
--
ALTER TABLE `addons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `business_settings`
--
ALTER TABLE `business_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `conversations`
--
ALTER TABLE `conversations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupon_usages`
--
ALTER TABLE `coupon_usages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customer_packages`
--
ALTER TABLE `customer_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer_products`
--
ALTER TABLE `customer_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `flash_deals`
--
ALTER TABLE `flash_deals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `flash_deal_products`
--
ALTER TABLE `flash_deal_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_categories`
--
ALTER TABLE `home_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pickup_points`
--
ALTER TABLE `pickup_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `policies`
--
ALTER TABLE `policies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `product_stocks`
--
ALTER TABLE `product_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `searches`
--
ALTER TABLE `searches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `seller_withdraw_requests`
--
ALTER TABLE `seller_withdraw_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `seo_settings`
--
ALTER TABLE `seo_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sub_sub_categories`
--
ALTER TABLE `sub_sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `ticket_replies`
--
ALTER TABLE `ticket_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wishlists`
--
ALTER TABLE `wishlists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
